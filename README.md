# Algorithms and Data Structures

This repository holds a collection of different algorithms and data structures
written in Java.

These were developed under great influence of "Algorithms" book by
Robert Sedgewick and Kevin Wayne.

# Development

[![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://lk-algorithms.gitlab.io/algorithms-and-data-structures)