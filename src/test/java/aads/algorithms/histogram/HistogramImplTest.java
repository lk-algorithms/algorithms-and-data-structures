package aads.algorithms.histogram;

import aads.algorithms.histogram.api.Histogram;
import aads.algorithms.histogram.implementation.HistogramImpl;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class HistogramImplTest {

    private Histogram h = new HistogramImpl();

    @Test
    public void testHistogram() {
        int[] nums = {2, 3, 4, 1, 0, 4, 1, 1, 2, 1, 6};
        int max = 8;
        int[] expected = {1, 4, 2, 1, 2, 0, 1, 0, 0};


        int[] actual = h.histogram(nums, max);

        assertEquals("nums size should equal sum over actual", nums.length, IntStream.of(actual).sum());
        assertArrayEquals("Histograms should match", expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMaxNegative() {
        int[] nums = {2, 3, 4};
        int max = -1;
        h.histogram(nums, max);
    }

    @Test(expected = RuntimeException.class)
    public void testNumExceedsMax() {
        int[] nums = {2, 3, 4};
        int max = 3;
        h.histogram(nums, max);
    }
}
