package aads.algorithms.search;

import aads.algorithms.search.implementation.RecursiveBinarySearch;

public class RecursiveSearchTest extends SearchTestBase<RecursiveBinarySearch<Integer>> {

    @Override
    protected RecursiveBinarySearch<Integer> createBinarySearch() {
        return new RecursiveBinarySearch<>();
    }

}
