package aads.algorithms.search;

import aads.algorithms.search.api.Search;
import aads.algorithms.search.implementation.IterativeBinarySearch;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class SearchTestBase<T extends Search<Integer>> {

    protected T bs;

    protected abstract T createBinarySearch();

    @Before
    public void setUp() {
        bs = createBinarySearch();
    }

    @Test
    public void testEmptyArrayLookup() {
        int idx = bs.indexOf(new Integer[0], 5);

        assertEquals(-1, idx);
    }

    @Test
    public void testSingleElementMismatch() {
        Integer[] arr = {1};
        int idx = bs.indexOf(arr, 5);

        assertEquals(-1, idx);
    }

    @Test
    public void testSingleElementMatch() {
        Integer[] arr = {5};
        int idx = bs.indexOf(arr, 5);

        assertEquals(0, idx);
    }

    @Test
    public void testManyElementsMatchFirst() {
        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 2);

        assertEquals(0, idx);
    }

    @Test
    public void testManyElementsMatchBegin() {
        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 4);

        assertEquals(1, idx);
    }

    @Test
    public void testManyElementsMatchMiddle() {
        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 10);

        assertEquals(4, idx);
    }

    @Test
    public void testManyElementsMatchEnd() {
        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 16);

        assertEquals(7, idx);
    }

    @Test
    public void testManyElementsMatchLast() {
        Search<Integer> bs = new IterativeBinarySearch<>();

        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 18);

        assertEquals(8, idx);
    }

    @Test
    public void testManyElementsMismatch() {
        Integer[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 18};
        int idx = bs.indexOf(arr, 13);

        assertEquals(-1, idx);
    }
}
