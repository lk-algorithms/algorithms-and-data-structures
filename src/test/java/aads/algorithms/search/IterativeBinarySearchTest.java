package aads.algorithms.search;

import aads.algorithms.search.implementation.IterativeBinarySearch;

public class IterativeBinarySearchTest extends SearchTestBase<IterativeBinarySearch<Integer>> {

    @Override
    protected IterativeBinarySearch<Integer> createBinarySearch() {
        return new IterativeBinarySearch<>();
    }

}
