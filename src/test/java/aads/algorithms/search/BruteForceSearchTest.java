package aads.algorithms.search;

import aads.algorithms.search.implementation.BruteForceSearch;

public class BruteForceSearchTest extends SearchTestBase<BruteForceSearch<Integer>> {

    @Override
    protected BruteForceSearch<Integer> createBinarySearch() {
        return new BruteForceSearch<>();
    }

}
