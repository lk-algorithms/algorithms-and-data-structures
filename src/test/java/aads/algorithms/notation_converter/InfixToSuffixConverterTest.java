package aads.algorithms.notation_converter;

import aads.algorithms.notation_converter.api.NotationConverter;
import aads.algorithms.notation_converter.implementation.InfixToSuffixConverter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InfixToSuffixConverterTest {

    @Test
    public void testInfixToSuffixSingleNumber() {
        NotationConverter nc = new InfixToSuffixConverter();

        String infix = "1";
        String suffix = "1";
        assertEquals(suffix, nc.convert(infix));
    }

    @Test
    public void testInfixToSuffixSimpleSum() {
        NotationConverter nc = new InfixToSuffixConverter();

        String infix = "(1+2)";
        String suffix = "12+";
        assertEquals(suffix, nc.convert(infix));
    }

    @Test
    public void testInfixToSuffixCompoundSum1() {
        NotationConverter nc = new InfixToSuffixConverter();

        String infix = "((1+2)+3)";
        String suffix = "12+3+";
        assertEquals(suffix, nc.convert(infix));
    }

    @Test
    public void testInfixToSuffixCompoundSum2() {
        NotationConverter nc = new InfixToSuffixConverter();

        String infix = "((1+2)+(3+4))";
        String suffix = "12+34++";
        assertEquals(suffix, nc.convert(infix));
    }

    @Test
    public void testInfixToSuffixComplexExpression() {
        NotationConverter nc = new InfixToSuffixConverter();

        String infix = "(((1+2)*3)/(4-5))";
        String suffix = "12+3*45-/";
        assertEquals(suffix, nc.convert(infix));
    }
}
