package aads.algorithms.three_sum;

import aads.algorithms.three_sum.api.ThreeSum;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class ThreeSumTestBase<T extends ThreeSum> {

    protected T ts;

    protected abstract T createThreeSum();

    @Before
    public void setUp() {
        ts = createThreeSum();
    }

    @Test
    public void testArrSizeLessThan3() {
        int[] arr1 = {};
        int[] arr2 = {1};
        int[] arr3 = {1, 2};

        assertEquals(0, ts.count(arr1));
        assertEquals(0, ts.count(arr2));
        assertEquals(0, ts.count(arr3));
    }

    @Test
    public void testSingleTripleMatch() {
        int[] arr = {2, 4, -6};

        assertEquals(1, ts.count(arr));
    }

    @Test
    public void testSingleTripleMismatch() {
        int[] arr = {1, -2, -10};

        assertEquals(0, ts.count(arr));
    }


    @Test
    public void testComplex() {
        int[] arr = {4, -1, -2, 8, -4, 0, -3, -5};

        assertEquals(3, ts.count(arr));
    }

    @Test
    public void testReallyComplex() {
        int[] arr = {-46, -45, -44, -34, -32, -31, -28,
                -22, -16, -13, -12, -7, -4, -3, 3,
                5, 15, 16, 17, 26, 27, 29, 35, 38,
                40, 41, 44, 45, 46, 49};

        assertEquals(38, ts.count(arr));
    }

}
