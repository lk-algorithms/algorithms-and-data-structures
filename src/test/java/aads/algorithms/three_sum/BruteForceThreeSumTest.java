package aads.algorithms.three_sum;

import aads.algorithms.three_sum.implementation.BruteForceThreeSum;

public class BruteForceThreeSumTest extends ThreeSumTestBase<BruteForceThreeSum> {

    @Override
    protected BruteForceThreeSum createThreeSum() {
        return new BruteForceThreeSum();
    }
}
