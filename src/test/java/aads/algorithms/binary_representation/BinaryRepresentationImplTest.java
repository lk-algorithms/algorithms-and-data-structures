package aads.algorithms.binary_representation;

import aads.algorithms.binary_representation.api.BinaryRepresentation;
import aads.algorithms.binary_representation.implementation.BinaryRepresentationImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinaryRepresentationImplTest {

    @Test
    public void testBinaryRepresentation() {
        BinaryRepresentation br = new BinaryRepresentationImpl();

        String binaryDec0 = br.toBinaryRepresentation((long) 0); // 0
        String binaryDec1 = br.toBinaryRepresentation((long) 1); // 1
        String binaryDec2 = br.toBinaryRepresentation((long) 2); // 10
        String binaryDec4 = br.toBinaryRepresentation((long) 4); // 100
        String binaryDec50 = br.toBinaryRepresentation((long) 50); // 110010
        String binaryDec1242 = br.toBinaryRepresentation((long) 1242); // 10011011010
        String binaryDec4358345 = br.toBinaryRepresentation((long) 4358345); // 10000101000000011001001

        assertEquals("0", binaryDec0);
        assertEquals("1", binaryDec1);
        assertEquals("10", binaryDec2);
        assertEquals("100", binaryDec4);
        assertEquals("110010", binaryDec50);
        assertEquals("10011011010", binaryDec1242);
        assertEquals("10000101000000011001001", binaryDec4358345);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testArgumentLessThanZeroException() {
        BinaryRepresentation br = new BinaryRepresentationImpl();

        br.toBinaryRepresentation((long) -1);
    }
}
