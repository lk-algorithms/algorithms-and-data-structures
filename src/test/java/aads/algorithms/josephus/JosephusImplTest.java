package aads.algorithms.josephus;

import aads.algorithms.josephus.api.Josephus;
import aads.algorithms.josephus.implementation.JosephusImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JosephusImplTest {

    @Test
    public void testJosephusSequences1() {
        Josephus josephus = new JosephusImpl();

        assertEquals("", josephus.josephus(1, 0));
    }

    @Test
    public void testJosephusSequences2() {
        Josephus josephus = new JosephusImpl();

        assertEquals("0", josephus.josephus(10, 1));
    }

    @Test
    public void testJosephusSequences3() {
        Josephus josephus = new JosephusImpl();

        assertEquals("0 1 2 3 4 5 6 7 8 9", josephus.josephus(1, 10));
    }

    @Test
    public void testJosephusSequences4() {
        Josephus josephus = new JosephusImpl();

        assertEquals("1 2 0", josephus.josephus(5, 3));
    }

    @Test
    public void testJosephusSequences5() {
        Josephus josephus = new JosephusImpl();

        assertEquals("1 3 5 0 4 2 6", josephus.josephus(2, 7));
    }

    @Test
    public void testJosephusSequences6() {
        Josephus josephus = new JosephusImpl();

        assertEquals("2 5 8 1 6 0 7 4 9 3", josephus.josephus(3, 10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMLessThan1Exception() {
        Josephus josephus = new JosephusImpl();

        josephus.josephus(0, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNLessThan0Exception() {
        Josephus josephus = new JosephusImpl();

        josephus.josephus(10, -1);
    }
}
