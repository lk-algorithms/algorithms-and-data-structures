package aads.algorithms.greatest_common_divisor;

import aads.algorithms.greatest_common_divisor.api.GreatestCommonDivisor;
import aads.algorithms.greatest_common_divisor.implementation.GreatestCommonDivisorImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GreatestCommonDivisorTest {

    private GreatestCommonDivisor gcd = new GreatestCommonDivisorImpl();

    @Test
    public void testGCD() {
        long gcd1 = gcd.gcd(1, 1);
        assertEquals(1, gcd1);

        long gcd2 = gcd.gcd(2, 1);
        assertEquals(1, gcd2);

        long gcd3 = gcd.gcd(1, 2);
        assertEquals(1, gcd3);

        long gcd4 = gcd.gcd(2, 4);
        assertEquals(2, gcd4);

        long gcd5 = gcd.gcd(4, 2);
        assertEquals(2, gcd5);

        long gcd6 = gcd.gcd(14, 63);
        assertEquals(7, gcd6);

        long gcd7 = gcd.gcd(32432, 6324);
        assertEquals(4, gcd7);

        long gcd8 = gcd.gcd(32432, 6324);
        assertEquals(4, gcd8);

        long gcd9 = gcd.gcd(1111111, 1234567);
        assertEquals(1, gcd9);

        long gcd10 = gcd.gcd(1111112, 1234560);
        assertEquals(8, gcd10);

        long gcd11 = gcd.gcd(11, 11);
        assertEquals(11, gcd11);
    }
}
