package aads.algorithms.union_find;

import aads.algorithms.union_find.api.UnionFind;
import org.junit.Test;

import static org.junit.Assert.*;

public abstract class UnionFindTestBase<T extends UnionFind> {

    abstract T createUFInstance(int n);

    @Test(expected = RuntimeException.class)
    public void testSitesLessThan1Exception() {
        createUFInstance(0);
    }

    /**
     * Tests reflexive property of UF
     */
    @Test
    public void testReflexive() {
        UnionFind uf = createUFInstance(1);

        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertTrue(uf.connected(0, 0));
        uf.union(0, 0);
        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertTrue(uf.connected(0, 0));
    }

    /**
     * Tests symmetric property of UF
     */
    @Test
    public void testTwoSites() {
        UnionFind uf = createUFInstance(2);

        // Initial environment
        assertEquals(2, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(1, uf.find(1));
        assertFalse(uf.connected(0, 1));
        assertFalse(uf.connected(1, 0));

        // Union
        uf.union(0, 1);

        // Environment after union
        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(0, uf.find(1));
        assertTrue(uf.connected(0, 1));
        assertTrue(uf.connected(1, 0));

        // Repeat union
        uf.union(0, 1);

        // Environment after repeated union
        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(0, uf.find(1));
        assertTrue(uf.connected(0, 1));
        assertTrue(uf.connected(1, 0));
    }

    /**
     * Tests transitive property of UF
     */
    @Test
    public void testTransitive() {
        UnionFind uf = createUFInstance(3);

        // Initial environment
        assertEquals(3, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(1, uf.find(1));
        assertEquals(2, uf.find(2));
        assertFalse(uf.connected(0, 1));
        assertFalse(uf.connected(1, 2));
        assertFalse(uf.connected(0, 2));

        // Union
        uf.union(0, 1);
        uf.union(1, 2);

        // Environment after union
        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(0, uf.find(1));
        assertEquals(0, uf.find(2));
        assertTrue(uf.connected(0, 1));
        assertTrue(uf.connected(1, 2));
        assertTrue(uf.connected(0, 2));

        // Repeat union
        uf.union(0, 1);
        uf.union(1, 2);

        // Environment after repeated union
        assertEquals(1, uf.count());
        assertEquals(0, uf.find(0));
        assertEquals(0, uf.find(1));
        assertEquals(0, uf.find(2));
        assertTrue(uf.connected(0, 1));
        assertTrue(uf.connected(1, 2));
        assertTrue(uf.connected(0, 2));
    }

}
