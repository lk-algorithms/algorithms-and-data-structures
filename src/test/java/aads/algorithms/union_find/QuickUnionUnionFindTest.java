package aads.algorithms.union_find;

import aads.algorithms.union_find.implementation.QuickUnionUnionFind;

public class QuickUnionUnionFindTest extends UnionFindTestBase<QuickUnionUnionFind> {

    @Override
    QuickUnionUnionFind createUFInstance(int n) {
        return new QuickUnionUnionFind(n);
    }
}
