package aads.algorithms.union_find;

import aads.algorithms.union_find.implementation.QuickFindUnionFind;

public class QuickFindUnionFindTest extends UnionFindTestBase<QuickFindUnionFind> {

    @Override
    QuickFindUnionFind createUFInstance(int n) {
        return new QuickFindUnionFind(n);
    }
}
