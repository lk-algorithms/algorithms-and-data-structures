package aads.algorithms.sort;

import aads.algorithms.sort.api.Sort;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public abstract class SortTestBase<T extends Sort<Integer>> {
    protected T sort;

    protected abstract T createSort();

    @Before
    public void setUp() {
        sort = createSort();
    }

    @Test
    public void testEmptyArray() {
        Integer[] arr = {};

        sort.sort(arr);

        assertArrayEquals(new Integer[]{}, arr);
    }

    @Test
    public void testSingleElementArray() {
        Integer[] arr = {1};

        sort.sort(arr);

        assertArrayEquals(new Integer[]{1}, arr);
    }

    @Test
    public void testTwoElementsArraySorted() {
        Integer[] arr = {1, 2};

        sort.sort(arr);

        assertArrayEquals(new Integer[]{1, 2}, arr);
    }

    @Test
    public void testTwoElementsArrayUnsorted() {
        Integer[] arr = {2, 1};

        sort.sort(arr);

        assertArrayEquals(new Integer[]{1, 2}, arr);
    }

    @Test
    public void testManyElementsArrayUnsorted() {
        Integer[] arr = {81, 66, 64, 91, 18, 48, 61, 27, 26, 8, 21, 24, 72, 69, 47, 58, 63, 84, 13, 89, 33, 56};
        Integer[] expected = {8, 13, 18, 21, 24, 26, 27, 33, 47, 48, 56, 58, 61, 63, 64, 66, 69, 72, 81, 84, 89, 91};

        sort.sort(arr);

        assertArrayEquals(expected, arr);
    }

    @Test
    public void testManyElementsArrayUnsortedDuplicated() {
        Integer[] arr = {81, 66, 64, 91, 18, 81, 61, 27, 26, 8, 21, 24, 8, 69, 47, 58, 63, 84, 13, 89, 33, 13};
        Integer[] expected = {8, 8, 13, 13, 18, 21, 24, 26, 27, 33, 47, 58, 61, 63, 64, 66, 69, 81, 81, 84, 89, 91};

        sort.sort(arr);

        assertArrayEquals(expected, arr);
    }
}
