package aads.algorithms.sort;

import aads.algorithms.sort.implementation.InsertionSort;

public class InsertionSortTest extends SortTestBase<InsertionSort<Integer>> {

    @Override
    protected InsertionSort<Integer> createSort() {
        return new InsertionSort<>();
    }

}
