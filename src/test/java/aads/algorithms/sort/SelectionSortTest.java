package aads.algorithms.sort;

import aads.algorithms.sort.implementation.SelectionSort;

public class SelectionSortTest extends SortTestBase<SelectionSort<Integer>> {

    @Override
    protected SelectionSort<Integer> createSort() {
        return new SelectionSort<>();
    }

}
