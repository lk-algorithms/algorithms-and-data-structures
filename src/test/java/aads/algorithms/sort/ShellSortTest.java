package aads.algorithms.sort;

import aads.algorithms.sort.implementation.ShellSort;

public class ShellSortTest extends SortTestBase<ShellSort<Integer>> {

    @Override
    protected ShellSort<Integer> createSort() {
        return new ShellSort<>();
    }

}
