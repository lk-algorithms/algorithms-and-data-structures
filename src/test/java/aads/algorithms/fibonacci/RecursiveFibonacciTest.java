package aads.algorithms.fibonacci;

import aads.algorithms.fibonacci.implementation.RecursiveFibonacci;

public class RecursiveFibonacciTest extends FibonacciTestBase<RecursiveFibonacci> {

    @Override
    protected RecursiveFibonacci createInstance() {
        return new RecursiveFibonacci();
    }
}
