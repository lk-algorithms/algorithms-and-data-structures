package aads.algorithms.fibonacci;

import aads.algorithms.fibonacci.api.Fibonacci;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class FibonacciTestBase<T extends Fibonacci> {

    private T instance;

    protected abstract T createInstance();

    @Before
    public void setUp() {
        instance = createInstance();
    }


    @Test
    public void testFibonacci() {
        assertEquals(0, instance.fibonacci(0));
        assertEquals(1, instance.fibonacci(1));
        assertEquals(1, instance.fibonacci(2));
        assertEquals(2, instance.fibonacci(3));
        assertEquals(3, instance.fibonacci(4));
        assertEquals(5, instance.fibonacci(5));
        assertEquals(8, instance.fibonacci(6));
        assertEquals(13, instance.fibonacci(7));
        assertEquals(21, instance.fibonacci(8));
        assertEquals(34, instance.fibonacci(9));
        assertEquals(55, instance.fibonacci(10));
        assertEquals(832040, instance.fibonacci(30));
    }
}
