package aads.algorithms.fibonacci;

import aads.algorithms.fibonacci.implementation.IterativeVarsFibonacci;

public class IterativeVarsFibonacciTest extends FibonacciTestBase<IterativeVarsFibonacci> {

    @Override
    protected IterativeVarsFibonacci createInstance() {
        return new IterativeVarsFibonacci();
    }
}
