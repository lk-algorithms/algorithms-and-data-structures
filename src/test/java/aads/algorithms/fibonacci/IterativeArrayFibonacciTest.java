package aads.algorithms.fibonacci;

import aads.algorithms.fibonacci.implementation.IterativeArrayFibonacci;

public class IterativeArrayFibonacciTest extends FibonacciTestBase<IterativeArrayFibonacci> {

    @Override
    protected IterativeArrayFibonacci createInstance() {
        return new IterativeArrayFibonacci();
    }
}
