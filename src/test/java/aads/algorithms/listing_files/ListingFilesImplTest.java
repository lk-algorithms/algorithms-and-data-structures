package aads.algorithms.listing_files;

import aads.algorithms.listing_files.api.ListingFiles;
import aads.algorithms.listing_files.exception.ListDirectoryIsFileException;
import aads.algorithms.listing_files.implementation.ListingFilesImpl;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListingFilesImplTest {

    @Rule
    public TemporaryFolder root = new TemporaryFolder();

    private ListingFiles lf = new ListingFilesImpl();

    @Test
    public void testEmpty() {
        String rootAbsPath = createStructure();

        assertEquals("", lf.list(rootAbsPath));
    }

    @Test
    public void testSingleFile() {
        String rootAbsPath = createStructure("file1.txt");

        assertEquals("file1.txt", lf.list(rootAbsPath));
    }

    @Test
    public void testSingleDir() {
        String rootAbsPath = createStructure("dir1");

        assertEquals("dir1", lf.list(rootAbsPath));
    }

    @Test
    public void testMultipleFiles() {
        String rootAbsPath = createStructure("file1.txt", "file2.txt", "file3.txt");

        assertEquals("file1.txt\nfile2.txt\nfile3.txt", lf.list(rootAbsPath));
    }

    @Test
    public void testMultipleDirs() {
        String rootAbsPath = createStructure("dir1", "dir2", "dir3");

        assertEquals("dir1\ndir2\ndir3", lf.list(rootAbsPath));
    }

    @Test
    public void testFlatIntermixed() {
        String rootAbsPath = createStructure("file1.txt", "dir1", "file2.txt", "dir2");

        assertEquals("dir1\ndir2\nfile1.txt\nfile2.txt", lf.list(rootAbsPath));
    }

    @Test
    public void testDeepDirs() {
        String rootAbsPath = createStructure("dir1", "dir1/dir2", "dir1/dir2/dir3");

        assertEquals("dir1\n    dir2\n        dir3", lf.list(rootAbsPath));
    }

    @Test
    public void testIntegration() {
        String rootAbsPath = createStructure("file1.txt", "file2.txt", "dir1", "dir2", "dir3", "dir2/file3.txt",
                "dir2/file4.txt", "dir3/file5.txt", "dir3/dir4", "dir3/dir4/file6.txt", "dir3/dir4/file7.txt");

        String expected = "" +
                "dir1\n" +
                "dir2\n" +
                "    file3.txt\n" +
                "    file4.txt\n" +
                "dir3\n" +
                "    dir4\n" +
                "        file6.txt\n" +
                "        file7.txt\n" +
                "    file5.txt\n" +
                "file1.txt\n" +
                "file2.txt";
        assertEquals(expected, lf.list(rootAbsPath));
    }

    @Test(expected = ListDirectoryIsFileException.class)
    public void testListFile() {
        String rootAbsPath = createStructure("file1.txt");
        String fileAbsPath = Paths.get(rootAbsPath, "file1.txt").toAbsolutePath().toString();

        lf.list(fileAbsPath);
    }


    private String createStructure(String... structure) {
        Path rootAbsPath = Paths.get(root.getRoot().getAbsolutePath());
        List<Path> filePaths = new ArrayList<>();
        List<Path> dirPaths = new ArrayList<>();

        for (String item : structure) {
            if (item.endsWith(".txt")) {
                filePaths.add(Paths.get(rootAbsPath.toString(), item));
            } else {
                dirPaths.add(Paths.get(rootAbsPath.toString(), item));
            }
        }

        try {
            createDirs(dirPaths);
            createFiles(filePaths);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rootAbsPath.toString();
    }

    private void createDirs(List<Path> dirPaths) throws IOException {
        for (Path dirPath : dirPaths) {
            Files.createDirectory(dirPath);
        }
    }

    private void createFiles(List<Path> filePaths) throws IOException {
        for (Path filePath : filePaths) {
            Files.createFile(filePath);
        }
    }
}
