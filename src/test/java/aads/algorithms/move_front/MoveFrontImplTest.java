package aads.algorithms.move_front;

import aads.algorithms.move_front.api.MoveFront;
import aads.algorithms.move_front.implementation.MoveFrontImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoveFrontImplTest {

    private MoveFront mv;

    @Before
    public void setup() {
        mv = new MoveFrontImpl();
    }

    @Test
    public void testMoveFrontEmpty() {
        assertEquals("", mv.buffer());
    }

    @Test
    public void testMoveFrontSingleChar() {
        assertFalse(mv.push('a'));
        assertEquals("a", mv.buffer());
    }

    @Test
    public void testMoveFrontVariousDifferentChars() {
        assertFalse(mv.push('a'));
        assertEquals("a", mv.buffer());

        assertFalse(mv.push('b'));
        assertEquals("b a", mv.buffer());

        assertFalse(mv.push('c'));
        assertEquals("c b a", mv.buffer());
    }

    @Test
    public void testMoveFrontSimpleCacheHit() {
        assertFalse(mv.push('a'));
        assertEquals("a", mv.buffer());

        assertTrue(mv.push('a'));
        assertEquals("a", mv.buffer());
    }

    @Test
    public void testMoveFrontSimpleMoveFront() {
        assertFalse(mv.push('a'));
        assertEquals("a", mv.buffer());

        assertFalse(mv.push('b'));
        assertEquals("b a", mv.buffer());

        assertTrue(mv.push('a'));
        assertEquals("a b", mv.buffer());
    }

    @Test
    public void testMoveFrontComplexSequence() {
        assertFalse(mv.push('a'));
        assertEquals("a", mv.buffer());

        assertFalse(mv.push('b'));
        assertEquals("b a", mv.buffer());

        assertFalse(mv.push('c'));
        assertEquals("c b a", mv.buffer());

        assertTrue(mv.push('b'));
        assertEquals("b c a", mv.buffer());

        assertTrue(mv.push('a'));
        assertEquals("a b c", mv.buffer());

        assertFalse(mv.push('d'));
        assertEquals("d a b c", mv.buffer());
    }
}
