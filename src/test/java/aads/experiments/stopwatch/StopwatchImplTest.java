package aads.experiments.stopwatch;

import aads.experiments.stopwatch.api.Stopwatch;
import aads.experiments.stopwatch.exception.StopwatchMultipleInitializationException;
import aads.experiments.stopwatch.implementation.StopwatchImpl;
import org.junit.Test;

import java.time.Clock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StopwatchImplTest {

    @Test
    public void testMeasurements() {
        Clock clock = mock(Clock.class);
        when(clock.millis()).thenReturn(3000L, 10000L, 15000L);

        Stopwatch stopwatch = new StopwatchImpl(clock);
        stopwatch.start();
        double m1 = stopwatch.elapsedTime();
        double m2 = stopwatch.elapsedTime();

        assertEquals(7.0, m1, 0);
        assertEquals(12.0, m2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullClockException() {
        new StopwatchImpl(null);
    }

    @Test(expected = StopwatchMultipleInitializationException.class)
    public void testStopwatchMultipleInitializationException() {
        Stopwatch stopwatch = new StopwatchImpl(Clock.systemDefaultZone());
        stopwatch.start();
        stopwatch.start();
    }

}
