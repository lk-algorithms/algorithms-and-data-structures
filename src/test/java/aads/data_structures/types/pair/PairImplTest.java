package aads.data_structures.types.pair;

import aads.data_structures.types.pair.api.Pair;
import aads.data_structures.types.pair.implementation.PairImpl;
import org.junit.Test;

import static org.junit.Assert.*;


public class PairImplTest {

    @Test
    public void testPairCreation() {
        Pair<String, Integer> pair = new PairImpl<>("first", 2);

        assertEquals("first", pair.first());
        assertEquals(Integer.valueOf(2), pair.second());
    }

    @Test
    public void testPairDisplay() {
        Pair<String, Integer> pair = new PairImpl<>("first", 2);

        assertEquals("<first, 2>", pair.toString());
    }


    @Test
    public void testPairEquality() {
        Pair<String, Integer> pair1 = new PairImpl<>("first", 2);
        Pair<String, Integer> pair2 = new PairImpl<>("first", 2);

        assertTrue(pair1.equals(pair2));
        assertTrue(pair2.equals(pair1));
        assertTrue(pair1.hashCode() == pair2.hashCode());
    }

    @Test
    public void testPairInequality() {
        Pair<String, Integer> pair1 = new PairImpl<>("first", 2);
        Pair<String, Integer> pair2 = new PairImpl<>("second", 1);

        assertFalse(pair1.equals(pair2));
        assertFalse(pair2.equals(pair1));
        assertFalse(pair1.hashCode() == pair2.hashCode());
    }

}
