package aads.data_structures.types.matrix;

import aads.data_structures.types.matrix.api.Matrix;
import aads.data_structures.types.matrix.exception.ArrayNotMatrixShapeException;
import aads.data_structures.types.matrix.exception.NotMatchingMatricesMultiplicationException;
import aads.data_structures.types.matrix.implementation.ArrayMatrix;
import org.junit.Test;

import static org.junit.Assert.*;


public class ArrayMatrixTest {

    // CREATION TESTS

    @Test
    public void testCreationEmptyArray() {
        Matrix m = new ArrayMatrix(new double[0][]);
        assertArrayEquals(new double[0][], m.toArray());
    }

    @Test
    public void testCreationRowVector() {
        Matrix m = new ArrayMatrix(new double[][]{{4, 5, 6}});
        assertArrayEquals(new double[][]{{4, 5, 6}}, m.toArray());
    }

    @Test
    public void testCreationColumnVector() {
        Matrix m = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        assertArrayEquals(new double[][]{{4}, {5}, {6}}, m.toArray());
    }

    @Test
    public void testCreationMatrix() {
        Matrix m = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertArrayEquals(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, m.toArray());
    }


    // DIMENSIONS TESTS
    @Test
    public void testCols() {
        Matrix m1 = new ArrayMatrix(new double[0][]);
        Matrix m2 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m3 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix m4 = new ArrayMatrix(new double[][]{{1, 2}, {4, 5}, {7, 8}});

        assertEquals(0, m1.cols());
        assertEquals(3, m2.cols());
        assertEquals(1, m3.cols());
        assertEquals(2, m4.cols());
    }

    @Test
    public void testRows() {
        Matrix m1 = new ArrayMatrix(new double[0][]);
        Matrix m2 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m3 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix m4 = new ArrayMatrix(new double[][]{{1, 2}, {4, 5}, {7, 8}});

        assertEquals(0, m1.rows());
        assertEquals(1, m2.rows());
        assertEquals(3, m3.rows());
        assertEquals(3, m4.rows());
    }

    @Test
    public void testEmptiness() {
        Matrix empty = new ArrayMatrix(new double[0][]);
        Matrix notEmpty = new ArrayMatrix(new double[][]{{4, 5, 6}});

        assertTrue(empty.isEmpty());
        assertFalse(notEmpty.isEmpty());
    }


    // DISPLAY TESTS

    @Test
    public void testDisplayEmptyMatrix() {
        Matrix m = new ArrayMatrix(new double[0][]);
        String expected = "{}";
        assertEquals(expected, m.toString());
    }

    @Test
    public void testDisplayRowVector() {
        Matrix m = new ArrayMatrix(new double[][]{{4, 5, 6}});
        String expected = "{ 4.00   5.00   6.00 }";
        assertEquals(expected, m.toString());
    }

    @Test
    public void testDisplayColumnVector() {
        Matrix m = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        String expected = "{ 4.00 }\n{ 5.00 }\n{ 6.00 }";
        assertEquals(expected, m.toString());
    }

    @Test
    public void testDisplayMatrix() {
        Matrix m = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        String expected = "{ 1.00   2.00   3.00 }\n{ 4.00   5.00   6.00 }\n{ 7.00   8.00   9.00 }";
        assertEquals(expected, m.toString());
    }


    // EQUALITY TESTS

    @Test
    public void testEqualityEmptyMatrices() {
        Matrix m1 = new ArrayMatrix(new double[0][]);
        Matrix m2 = new ArrayMatrix(new double[0][]);
        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));
    }

    @Test
    public void testEqualityRowVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));
    }

    @Test
    public void testEqualityColumnVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));
    }

    @Test
    public void testEqualityMatrices() {
        Matrix m1 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        Matrix m2 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));
    }


    // INEQUALITY TESTS

    @Test
    public void testInequalitySameSizeRowVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4, 8, 6}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }

    @Test
    public void testInequalityDifferentSizeRowVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4, 5, 6, 7}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }

    @Test
    public void testInequalitySameSizeColumnVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4}, {5}, {9}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }

    @Test
    public void testInequalityDifferentSizeColumnVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4}, {5}, {6}, {7}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }

    @Test
    public void testInequalitySameSizeMatrices() {
        Matrix m1 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 2, 6}, {7, 8, 9}});
        Matrix m2 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }

    @Test
    public void testInequalityDifferentSizeMatrices() {
        Matrix m1 = new ArrayMatrix(new double[][]{{1, 2}, {4, 5}, {7, 8}});
        Matrix m2 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertFalse(m1.equals(m2));
        assertFalse(m2.equals(m1));
    }


    // TRANSPOSITION TESTS

    @Test
    public void testTransposeEmpty() {
        Matrix input = new ArrayMatrix(new double[0][]);
        Matrix output = input.transpose();
        assertEquals(new ArrayMatrix(new double[0][]), output);
    }

    @Test
    public void testTransposeRowVector() {
        Matrix input = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix output = input.transpose();
        assertEquals(new ArrayMatrix(new double[][]{{4}, {5}, {6}}), output);
    }

    @Test
    public void testTransposeColumnVector() {
        Matrix input = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix output = input.transpose();
        assertEquals(new ArrayMatrix(new double[][]{{4, 5, 6}}), output);
    }

    @Test
    public void testTransposeMatrix() {
        Matrix input = new ArrayMatrix(new double[][]{{1, 2}, {4, 5}, {7, 8}});
        Matrix output = input.transpose();
        assertEquals(new ArrayMatrix(new double[][]{{1, 4, 7}, {2, 5, 8}}), output);
    }


    // MULTIPLICATION TESTS

    @Test
    public void testMultiplicationEmptyMatrices() {
        Matrix m1 = new ArrayMatrix(new double[0][]);
        Matrix m2 = new ArrayMatrix(new double[0][]);
        Matrix output = m1.mult(m2);
        assertEquals(new ArrayMatrix(new double[0][]), output);
    }

    @Test
    public void testMultiplicationVectors() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4, 5, 6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});

        Matrix output1 = m1.mult(m2);
        assertEquals(new ArrayMatrix(new double[][]{{77}}), output1);

        Matrix output2 = m2.mult(m1);
        assertEquals(new ArrayMatrix(new double[][]{{16, 20, 24}, {20, 25, 30}, {24, 30, 36}}), output2);
    }

    @Test
    public void testMultiplicationMatrices() {
        Matrix m1 = new ArrayMatrix(new double[][]{{1, 2}, {3, 4}, {5, 6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}});

        Matrix output1 = m1.mult(m2);
        assertEquals(new ArrayMatrix(new double[][]{{9, 12, 15}, {19, 26, 33}, {29, 40, 51}}), output1);

        Matrix output2 = m2.mult(m1);
        assertEquals(new ArrayMatrix(new double[][]{{22, 28}, {49, 64}}), output2);
    }


    // EXCEPTIONS TESTS

    @Test(expected = ArrayNotMatrixShapeException.class)
    public void testInvalidCreationEmptyArray() {
       new ArrayMatrix(new double[][]{{}});
    }

    @Test(expected = ArrayNotMatrixShapeException.class)
    public void testInvalidCreationUnevenArray() {
        new ArrayMatrix(new double[][]{{1}, {1, 2}, {1, 2, 3}});
    }

    @Test(expected = NotMatchingMatricesMultiplicationException.class)
    public void testNotMatchingMatrixMultiplicationException() {
        Matrix m1 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        Matrix m2 = new ArrayMatrix(new double[][]{{4}, {5}, {6}});
        m1.mult(m2);
    }
}
