package aads.data_structures.types.rational;

import aads.data_structures.types.rational.api.Rational;
import aads.data_structures.types.rational.implementation.RationalImpl;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class RationalImplTest {

    @Test
    public void test2Over5() {
        Rational rational = new RationalImpl(2, 5);

        assertEquals("2/5", rational.toString());
    }

    @Test
    public void testMin2Over5() {
        Rational rational = new RationalImpl(-2, 5);

        assertEquals("-2/5", rational.toString());
    }

    @Test
    public void test2OverMin5() {
        Rational rational = new RationalImpl(2, -5);

        assertEquals("-2/5", rational.toString());
    }

    @Test
    public void testMin2OverMin5() {
        Rational rational = new RationalImpl(-2, -5);

        assertEquals("2/5", rational.toString());
    }

    @Test
    public void test2Over4() {
        Rational rational = new RationalImpl(2, 4);

        assertEquals("1/2", rational.toString());
    }

    @Test
    public void testMin2Over4() {
        Rational rational = new RationalImpl(-2, 4);

        assertEquals("-1/2", rational.toString());
    }

    @Test
    public void test2OverMin4() {
        Rational rational = new RationalImpl(2, -4);

        assertEquals("-1/2", rational.toString());
    }

    @Test
    public void testMin2OverMin4() {
        Rational rational = new RationalImpl(-2, -4);

        assertEquals("1/2", rational.toString());
    }

    @Test
    public void testEqualitySimple() {
        Rational r1 = new RationalImpl(6, 5);
        Rational r2 = new RationalImpl(6, 5);

        assertTrue(r1.equals(r2));
        assertTrue(r2.equals(r1));
        assertTrue(r1.hashCode() == r2.hashCode());
    }

    @Test
    public void testEqualityComplex() {
        Rational r1 = new RationalImpl(10, 5);
        Rational r2 = new RationalImpl(20, 10);

        assertTrue(r1.equals(r2));
        assertTrue(r2.equals(r1));
        assertTrue(r1.hashCode() == r2.hashCode());
    }

    @Test
    public void testInequality() {
        Rational r1 = new RationalImpl(7, 3);
        Rational r2 = new RationalImpl(3, 7);

        assertFalse(r1.equals(r2));
        assertFalse(r2.equals(r1));
        assertFalse(r1.hashCode() == r2.hashCode());
    }

    @Test
    public void testAdd() {
        Rational r1 = new RationalImpl(7, 3);
        Rational r2 = new RationalImpl(3, 7);

        Rational res = r1.plus(r2);
        assertEquals("58/21", res.toString());
    }

    @Test
    public void testMinus() {
        Rational r1 = new RationalImpl(7, -3);
        Rational r2 = new RationalImpl(3, 7);

        Rational res = r1.minus(r2);
        assertEquals("-58/21", res.toString());
    }

    @Test
    public void testMultiplication() {
        Rational r1 = new RationalImpl(7, -3);
        Rational r2 = new RationalImpl(3, 7);

        Rational res = r1.times(r2);
        assertEquals("-1/1", res.toString());
    }

    @Test
    public void testDivision() {
        Rational r1 = new RationalImpl(7, -3);
        Rational r2 = new RationalImpl(3, 7);

        Rational res = r1.dividedBy(r2);
        assertEquals("-49/9", res.toString());
    }
}
