package aads.data_structures.types.counter;

import aads.data_structures.types.counter.api.Counter;
import aads.data_structures.types.counter.implementation.CounterImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CounterImplTest {

    @Test
    public void testInitialCounter() {
        Counter counter = new CounterImpl();

        assertEquals(0, counter.tally());
    }

    @Test
    public void testCounterIncrement() {
        Counter counter = new CounterImpl();

        assertEquals(0, counter.tally());
        counter.increment();
        assertEquals(1, counter.tally());
        counter.increment();
        assertEquals(2, counter.tally());
    }

    @Test
    public void testCounterStringRepresentation() {
        Counter counter = new CounterImpl();

        assertEquals("Counter: 0", counter.toString());
        counter.increment();
        assertEquals("Counter: 1", counter.toString());
    }

}
