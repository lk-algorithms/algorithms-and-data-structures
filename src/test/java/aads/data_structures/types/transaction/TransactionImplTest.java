package aads.data_structures.types.transaction;

import aads.data_structures.types.date.implementation.MDYDate;
import aads.data_structures.types.transaction.exception.TransactionParsingException;
import aads.data_structures.types.transaction.implementation.TransactionImpl;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TransactionImplTest {

    private TransactionImpl createTransaction(String who, String when, int amount) {
        return new TransactionImpl(who, new MDYDate(when), amount);
    }

    private TransactionImpl createTransaction(String transaction) {
        return new TransactionImpl(transaction);
    }

    @Test
    public void testTransactionCreation() {
        TransactionImpl transaction = createTransaction("Smith", "1/1/2000", 100);

        assertEquals("Smith", transaction.who());
        assertEquals(new MDYDate("1/1/2000"), transaction.when());
        assertEquals(100.0, transaction.amount(), 0);
    }

    @Test
    public void testProperTransactionParsingConstructor() {
        TransactionImpl transaction = createTransaction("Turing 5/22/2000 11.99");

        assertEquals("Turing", transaction.who());
        assertEquals(new MDYDate("5/22/2000"), transaction.when());
        assertEquals(11.99, transaction.amount(), 0);
    }

    @Test(expected = TransactionParsingException.class)
    public void testInvalidTransactionParsingConstructor1() {
        createTransaction("Turing 5/22/2000");
    }

    @Test(expected = TransactionParsingException.class)
    public void testInvalidTransactionParsingConstructor2() {
        createTransaction("Turing 5/22/2000 11.99 something");
    }

    @Test(expected = TransactionParsingException.class)
    public void testInvalidTransactionParsingConstructor3() {
        createTransaction("Turing 5/g/2000 11.99 something");
    }

    @Test(expected = TransactionParsingException.class)
    public void testInvalidTransactionParsingConstructor4() {
        createTransaction("Turing 5/22/2000 11.9z9 something");
    }

    @Test
    public void testTransactionDisplay() {
        TransactionImpl transaction = createTransaction("Smith", "1/1/2000", 100);

        assertEquals("Smith 1/1/2000 100.00", transaction.toString());
    }

    @Test
    public void testTransactionEquality() {
        TransactionImpl transaction1 = createTransaction("Smith", "1/1/2000", 100);
        TransactionImpl transaction2 = createTransaction("Smith", "1/1/2000", 100);

        assertTrue(transaction1.equals(transaction2));
        assertTrue(transaction2.equals(transaction1));
        assertTrue(transaction1.hashCode() == transaction2.hashCode());
    }

    @Test
    public void testTransactionInequality1() {
        TransactionImpl transaction1 = createTransaction("Smith", "1/1/2000", 100);
        TransactionImpl transaction2 = createTransaction("Smithson", "1/1/2000", 100);

        assertFalse(transaction1.equals(transaction2));
        assertFalse(transaction2.equals(transaction1));
        assertFalse(transaction1.hashCode() == transaction2.hashCode());
    }

    @Test
    public void testTransactionInequality2() {
        TransactionImpl transaction1 = createTransaction("Smith", "1/1/2000", 100);
        TransactionImpl transaction2 = createTransaction("Smith", "1/1/2000", 101);

        assertFalse(transaction1.equals(transaction2));
        assertFalse(transaction2.equals(transaction1));
        assertFalse(transaction1.hashCode() == transaction2.hashCode());
    }

    @Test
    public void testTransactionInequality3() {
        TransactionImpl transaction1 = createTransaction("Smith", "1/1/2000", 100);
        TransactionImpl transaction2 = createTransaction("Smith", "1/1/2001", 100);

        assertFalse(transaction1.equals(transaction2));
        assertFalse(transaction2.equals(transaction1));
        assertFalse(transaction1.hashCode() == transaction2.hashCode());
    }

    @Test
    public void testTransactionOrdering() {
        TransactionImpl transaction1 = createTransaction("BBB", "1/1/2000", 100);
        TransactionImpl transaction2 = createTransaction("BBB", "1/1/2000", 100);
        TransactionImpl transaction3 = createTransaction("CCC", "1/1/2000", 1000);
        TransactionImpl transaction4 = createTransaction("CCC", "1/1/2000", 9999);
        TransactionImpl transaction5 = createTransaction("AAA", "1/2/2000", 10);

        assertTrue(transaction1.compareTo(transaction2) == 0);
        assertTrue(transaction2.compareTo(transaction3) < 0);
        assertTrue(transaction3.compareTo(transaction4) < 0);
        assertTrue(transaction4.compareTo(transaction5) < 0);
        assertTrue(transaction5.compareTo(transaction1) > 0);
    }

}
