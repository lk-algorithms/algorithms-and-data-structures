package aads.data_structures.types.date;

import aads.data_structures.types.date.implementation.MDYDate;

public class MDYDateTest extends DateTestBase<MDYDate> {

    @Override
    protected MDYDate createDate(int month, int day, int year) {
        return new MDYDate(month, day, year);
    }

    @Override
    protected MDYDate createDate(String date) {
        return new MDYDate(date);
    }

}
