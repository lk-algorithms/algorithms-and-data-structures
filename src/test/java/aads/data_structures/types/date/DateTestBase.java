package aads.data_structures.types.date;

import aads.data_structures.types.date.api.Date;
import aads.data_structures.types.date.exception.DateBeforeEpochStartException;
import aads.data_structures.types.date.exception.DateParsingException;
import aads.data_structures.types.date.exception.DayOutOfBoundsException;
import aads.data_structures.types.date.exception.MonthOutOfBoundsException;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public abstract class DateTestBase<T extends Date> {

    protected abstract T createDate(int month, int day, int year);

    protected abstract T createDate(String date);

    // SANITY CHECK TESTS

    @Test
    public void testDateCreation() {
        T date = createDate(1, 1, 2010);

        assertEquals(1, date.month());
        assertEquals(1, date.day());
        assertEquals(2010, date.year());
    }

    @Test
    public void testProperDateParsingConstructor() {
        T date = createDate("1/1/2010");

        assertEquals(1, date.month());
        assertEquals(1, date.day());
        assertEquals(2010, date.year());
    }

    @Test(expected = DateParsingException.class)
    public void testInvalidDateParsingConstructor1() {
        createDate("1/2010");
    }

    @Test(expected = DateParsingException.class)
    public void testInvalidDateParsingConstructor2() {
        createDate("1/1/2010/g");
    }

    @Test(expected = DateParsingException.class)
    public void testInvalidDateParsingConstructor3() {
        createDate("1/z/2010");
    }

    @Test
    public void testDateDisplay() {
        T date = createDate(1, 1, 2010);

        assertEquals("1/1/2010", date.toString());
    }

    @Test
    public void testDateEquality() {
        T date1 = createDate(1, 1, 2010);
        T date2 = createDate(1, 1, 2010);

        assertTrue(date1.equals(date2));
        assertTrue(date2.equals(date1));
        assertTrue(date1.hashCode() == date2.hashCode());
    }

    @Test
    public void testDateInequality() {
        T date1 = createDate(1, 1, 2010);
        T date2 = createDate(12, 12, 2012);

        assertFalse(date1.equals(date2));
        assertFalse(date2.equals(date1));
        assertFalse(date1.hashCode() == date2.hashCode());
    }

    @Test
    public void testDateOrdering() {
        T date1 = createDate(1, 2, 2010);
        T date2 = createDate(2, 1, 2010);
        T date3 = createDate(2, 2, 2010);
        T date4 = createDate(1, 1, 2011);

        assertTrue(date1.compareTo(date2) < 0);
        assertTrue(date2.compareTo(date3) < 0);
        assertTrue(date3.compareTo(date4) < 0);
        assertTrue(date4.compareTo(date1) > 0);
    }

    @Test
    public void testDayOfWeek() {
        T dateMonday = createDate(10, 8, 2018);
        T dateTuesday = createDate(3, 12, 2019);
        T dateWednesday = createDate(2, 15, 2017);
        T dateThursday = createDate(6, 14, 2018);
        T dateFriday = createDate(9, 18, 2020);
        T dateSaturday = createDate(5, 14, 2016);
        T dateSunday = createDate(11, 28, 2021);

        assertEquals("Monday", dateMonday.dayOfWeek());
        assertEquals("Tuesday", dateTuesday.dayOfWeek());
        assertEquals("Wednesday", dateWednesday.dayOfWeek());
        assertEquals("Thursday", dateThursday.dayOfWeek());
        assertEquals("Friday", dateFriday.dayOfWeek());
        assertEquals("Saturday", dateSaturday.dayOfWeek());
        assertEquals("Sunday", dateSunday.dayOfWeek());
    }

    // EDGE-CASES TESTS

    @Test
    public void testFirstDayOfEpoch() {
        T date = createDate(1, 1, 1970);

        assertEquals("1/1/1970", date.toString());
    }

    @Test(expected = DateBeforeEpochStartException.class)
    public void testDateBeforeEpochStartException() {
        createDate(12, 31, 1969);
    }

    @Test(expected = MonthOutOfBoundsException.class)
    public void test13thMonthException() {
        createDate(13, 1, 2000);
    }

    @Test(expected = MonthOutOfBoundsException.class)
    public void test0thMonthException() {
        createDate(0, 1, 2000);
    }

    @Test
    public void testRegularYearMaxDays() {
        List<T> dates = Arrays.asList(
                createDate(1, 31, 2003),
                createDate(2, 28, 2003),
                createDate(3, 31, 2003),
                createDate(4, 30, 2003),
                createDate(5, 31, 2003),
                createDate(6, 30, 2003),
                createDate(7, 31, 2003),
                createDate(8, 31, 2003),
                createDate(9, 30, 2003),
                createDate(10, 31, 2003),
                createDate(11, 30, 2003),
                createDate(12, 31, 2003)
        );
        List<Integer> expectedDaysInMonths = Arrays.asList(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        assertEquals(expectedDaysInMonths.size(), dates.size());
        for (int i = 0; i < expectedDaysInMonths.size(); i++) {
            assertEquals((int) expectedDaysInMonths.get(i), dates.get(i).day());
        }
    }

    @Test
    public void testLeapYearMaxDays() {
        List<T> dates = Arrays.asList(
                createDate(1, 31, 2008),
                createDate(2, 29, 2008),
                createDate(3, 31, 2008),
                createDate(4, 30, 2008),
                createDate(5, 31, 2008),
                createDate(6, 30, 2008),
                createDate(7, 31, 2008),
                createDate(8, 31, 2008),
                createDate(9, 30, 2008),
                createDate(10, 31, 2008),
                createDate(11, 30, 2008),
                createDate(12, 31, 2008)
        );
        List<Integer> expectedDaysInMonths = Arrays.asList(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        assertEquals(expectedDaysInMonths.size(), dates.size());
        for (int i = 0; i < expectedDaysInMonths.size(); i++) {
            assertEquals((int) expectedDaysInMonths.get(i), dates.get(i).day());
        }
    }

    @Test(expected = DayOutOfBoundsException.class)
    public void test0thDayException() {
        createDate(1, 0, 2000);
    }

    @Test
    public void testRegularYearFebruary() {
        Date date1 = createDate(2, 28, 2001);
        assertEquals("2/28/2001", date1.toString());

        Date date2 = createDate(2, 28, 2100);
        assertEquals("2/28/2100", date2.toString());
    }

    @Test(expected = DayOutOfBoundsException.class)
    public void testRegularYearFebruaryException1() {
        createDate(2, 29, 2001);
    }

    @Test(expected = DayOutOfBoundsException.class)
    public void testRegularYearFebruaryException2() {
        createDate(2, 29, 2100);
    }

    @Test
    public void testLeapYearFebruary() {
        Date date1 = createDate(2, 29, 2004);
        assertEquals("2/29/2004", date1.toString());

        Date date2 = createDate(2, 29, 2000);
        assertEquals("2/29/2000", date2.toString());
    }

    @Test(expected = DayOutOfBoundsException.class)
    public void testLeapYearFebruaryException1() {
        createDate(2, 30, 2004);
    }

    @Test(expected = DayOutOfBoundsException.class)
    public void testLeapYearFebruaryException2() {
        createDate(2, 30, 2000);
    }

}
