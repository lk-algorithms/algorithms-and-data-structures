package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.implementation.GeneralizedArrayQueue;

public class GeneralizedArrayQueueTest extends GeneralizedQueueTestBase<GeneralizedArrayQueue<String>> {

    protected GeneralizedArrayQueue<String> createQueueInstance() {
        return new GeneralizedArrayQueue<>();
    }

}
