package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.api.Queue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class QueueTestBase<T extends Queue<String>> {

    protected T queue;

    protected abstract T createQueueInstance();

    @Before
    public void setUp() {
        queue = createQueueInstance();
    }

    @Test
    public void testInitiallyQueueIsEmpty() {
        assertTrue("Initially queue should be empty", queue.isEmpty());
        assertEquals("Initially queue has size = 0", 0, queue.size());
    }

    @Test
    public void testEnqueueIncreasesQueueSize() {
        assertEquals(0, queue.size());
        queue.enqueue("To");
        assertEquals("enqueue() should increase queue size", 1, queue.size());
    }

    @Test
    public void testDequeueDecreasesQueueSize() {
        queue.enqueue("To");
        assertEquals(1, queue.size());

        queue.enqueue("be");
        assertEquals(2, queue.size());

        queue.dequeue();
        assertEquals("dequeue() should decrease queue size", 1, queue.size());
    }

    @Test
    public void testMixedEnqueueDequeue() {
        queue.enqueue("To");
        assertEquals(1, queue.size());

        queue.enqueue("be");
        assertEquals(2, queue.size());

        queue.enqueue("or");
        assertEquals(3, queue.size());

        assertEquals("To", queue.dequeue());
        assertEquals(2, queue.size());

        assertEquals("be", queue.dequeue());
        assertEquals(1, queue.size());

        queue.enqueue("not");
        assertEquals(2, queue.size());

        assertEquals("or", queue.dequeue());
        assertEquals(1, queue.size());
    }

    @Test(expected = QueueDeleteOutOfBoundsException.class)
    public void testDequeueEmptyQueueException() {
        queue.dequeue();
    }
}
