package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.implementation.LinkedListQueue;

public class LinkedListQueueTest extends QueueTestBase<LinkedListQueue<String>> {

    protected LinkedListQueue<String> createQueueInstance() {
        return new LinkedListQueue<String>();
    }

}