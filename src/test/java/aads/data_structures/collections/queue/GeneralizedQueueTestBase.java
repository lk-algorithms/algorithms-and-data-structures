package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.api.GeneralizedQueue;
import aads.data_structures.collections.queue.api.Queue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;
import aads.data_structures.collections.queue.implementation.GeneralizedArrayQueue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class GeneralizedQueueTestBase<T extends GeneralizedQueue<String>> extends QueueTestBase<T> {

    @Test(expected = QueueDeleteOutOfBoundsException.class)
    public void testDeleteIndexLowerThan0Exception() {
        queue.delete(-1);
    }

    @Test(expected = QueueDeleteOutOfBoundsException.class)
    public void testDeleteIndexGreaterEqualSizeException() {
        queue.enqueue("To");
        queue.delete(1);
    }

    @Test
    public void testDelete() {
        queue.enqueue("To");
        queue.enqueue("be");
        queue.enqueue("or");
        queue.enqueue("not");
        queue.enqueue("to");
        queue.enqueue("be");
        assertEquals(6, queue.size());

        assertEquals("be", queue.delete(5));
        assertEquals(5, queue.size());

        assertEquals("or", queue.delete(2));
        assertEquals(4, queue.size());

        assertEquals("not", queue.delete(2));
        assertEquals(3, queue.size());

        assertEquals("To", queue.delete(0));
        assertEquals(2, queue.size());

        assertEquals("to", queue.delete(1));
        assertEquals(1, queue.size());

        assertEquals("be", queue.delete(0));
        assertEquals(0, queue.size());
    }

}
