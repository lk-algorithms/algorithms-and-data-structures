package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.implementation.GeneralizedLinkedListQueue;

public class GeneralizedLinkedListQueueTest extends GeneralizedQueueTestBase<GeneralizedLinkedListQueue<String>> {

    protected GeneralizedLinkedListQueue<String> createQueueInstance() {
        return new GeneralizedLinkedListQueue<>();
    }

}
