package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.api.FixedCapacityQueue;
import aads.data_structures.collections.queue.exception.EnqueueFullQueueException;
import aads.data_structures.collections.queue.implementation.FixedArrayQueue;
import org.junit.Test;

import static org.junit.Assert.*;

public class FixedArrayQueueTest extends QueueTestBase<FixedArrayQueue<String>> {

    protected FixedArrayQueue<String> createQueueInstance() {
        int capacity = 10;
        return new FixedArrayQueue<String>(capacity);
    }

    @Test
    public void testExerciseSmallQueue() {
        int capacity = 3;
        queue = new FixedArrayQueue<String>(capacity);

        queue.enqueue("To");
        assertEquals(1, queue.size());

        queue.enqueue("be");
        assertEquals(2, queue.size());

        queue.enqueue("or");
        assertEquals(3, queue.size());

        assertEquals("To", queue.dequeue());
        assertEquals(2, queue.size());

        assertEquals("be", queue.dequeue());
        assertEquals(1, queue.size());

        queue.enqueue("not");
        assertEquals(2, queue.size());

        queue.enqueue("to");
        assertEquals(3, queue.size());

        assertEquals("or", queue.dequeue());
        assertEquals(2, queue.size());

        assertEquals("not", queue.dequeue());
        assertEquals(1, queue.size());

        assertEquals("to", queue.dequeue());
        assertEquals(0, queue.size());
    }

    @Test
    public void testFullQueue() {
        int capacity = 2;
        queue = new FixedArrayQueue<String>(capacity);

        queue.enqueue("To");
        assertEquals(1, queue.size());

        queue.enqueue("be");
        assertEquals(2, queue.size());

        assertTrue("Queue should be full", queue.isFull());
        assertEquals("Queue capacity = 2", 2, queue.capacity());
        assertFalse("Queue shouldn't be empty", queue.isEmpty());
    }

    @Test
    public void testFixedCapacity() {
        FixedCapacityQueue<String> queue1 = new FixedArrayQueue<String>(-1);
        FixedCapacityQueue<String> queue2 = new FixedArrayQueue<String>(0);
        FixedCapacityQueue<String> queue3 = new FixedArrayQueue<String>(1);
        FixedCapacityQueue<String> queue4 = new FixedArrayQueue<String>(2);

        assertEquals(1, queue1.capacity());
        assertEquals(1, queue2.capacity());
        assertEquals(1, queue3.capacity());
        assertEquals(2, queue4.capacity());
    }

    @Test(expected = EnqueueFullQueueException.class)
    public void testEnqueueFullQueueException() {
        int capacity = 1;
        FixedCapacityQueue<String> queue = new FixedArrayQueue<String>(capacity);
        queue.enqueue("To");
        queue.enqueue("be");
    }
}
