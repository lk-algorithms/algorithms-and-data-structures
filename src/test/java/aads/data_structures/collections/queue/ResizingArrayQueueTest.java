package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.implementation.ResizingArrayQueue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResizingArrayQueueTest extends QueueTestBase<ResizingArrayQueue<String>> {

    protected ResizingArrayQueue<String> createQueueInstance() {
        return new ResizingArrayQueue<String>();
    }

    @Test
    public void testChangingCapacity() {
        assertEquals(0, queue.size());
        assertEquals(1, queue.capacity());

        queue.enqueue("To");
        assertEquals(1, queue.size());
        assertEquals(1, queue.capacity());

        queue.enqueue("be");
        assertEquals(2, queue.size());
        assertEquals(2, queue.capacity());

        queue.enqueue("or");
        assertEquals(3, queue.size());
        assertEquals(4, queue.capacity());

        queue.enqueue("not");
        assertEquals(4, queue.size());
        assertEquals(4, queue.capacity());

        queue.enqueue("to");
        assertEquals(5, queue.size());
        assertEquals(8, queue.capacity());

        queue.dequeue();
        assertEquals(4, queue.size());
        assertEquals(8, queue.capacity());

        queue.dequeue();
        assertEquals(3, queue.size());
        assertEquals(8, queue.capacity());

        queue.dequeue();
        assertEquals(2, queue.size());
        assertEquals(4, queue.capacity());

        queue.dequeue();
        assertEquals(1, queue.size());
        assertEquals(2, queue.capacity());

        queue.dequeue();
        assertEquals(0, queue.size());
        assertEquals(2, queue.capacity());
    }
}
