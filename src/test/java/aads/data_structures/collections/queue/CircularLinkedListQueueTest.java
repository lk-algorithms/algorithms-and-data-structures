package aads.data_structures.collections.queue;

import aads.data_structures.collections.queue.implementation.CircularLinkedListQueue;

public class CircularLinkedListQueueTest extends QueueTestBase<CircularLinkedListQueue<String>> {

    protected CircularLinkedListQueue<String> createQueueInstance() {
        return new CircularLinkedListQueue<String>();
    }

}