package aads.data_structures.collections.stack;

import aads.data_structures.collections.stack.implementation.ResizingArrayStack;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResizingArrayStackTest extends StackTestBase<ResizingArrayStack<String>> {

    protected ResizingArrayStack<String> createStackInstance() {
        return new ResizingArrayStack<String>();
    }

    @Test
    public void testChangingCapacity() {
        assertEquals(0, stack.size());
        assertEquals(1, stack.capacity());

        stack.push("To");
        assertEquals(1, stack.size());
        assertEquals(1, stack.capacity());

        stack.push("be");
        assertEquals(2, stack.size());
        assertEquals(2, stack.capacity());

        stack.push("or");
        assertEquals(3, stack.size());
        assertEquals(4, stack.capacity());

        stack.push("not");
        assertEquals(4, stack.size());
        assertEquals(4, stack.capacity());

        stack.push("to");
        assertEquals(5, stack.size());
        assertEquals(8, stack.capacity());

        stack.pop();
        assertEquals(4, stack.size());
        assertEquals(8, stack.capacity());

        stack.pop();
        assertEquals(3, stack.size());
        assertEquals(8, stack.capacity());

        stack.pop();
        assertEquals(2, stack.size());
        assertEquals(4, stack.capacity());

        stack.pop();
        assertEquals(1, stack.size());
        assertEquals(2, stack.capacity());

        stack.pop();
        assertEquals(0, stack.size());
        assertEquals(2, stack.capacity());
    }
}
