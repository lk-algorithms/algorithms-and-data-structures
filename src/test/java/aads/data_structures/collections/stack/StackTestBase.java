package aads.data_structures.collections.stack;

import aads.data_structures.collections.stack.api.Stack;
import aads.data_structures.collections.stack.exception.PeekEmptyStackException;
import aads.data_structures.collections.stack.exception.PopEmptyStackException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class StackTestBase<T extends Stack<String>> {

    protected T stack;

    protected abstract T createStackInstance();

    @Before
    public void setUp() {
        stack = createStackInstance();
    }

    @Test
    public void testInitiallyStackIsEmpty() {
        assertTrue("Initially stack should be empty", stack.isEmpty());
        assertEquals("Initially stack has size = 0", 0, stack.size());
    }

    @Test
    public void testPushIncreasesStackSize() {
        assertEquals(0, stack.size());
        stack.push("To");
        assertEquals("push() should increase stack size", 1, stack.size());
    }

    @Test
    public void testPopDecreasesStackSize() {
        stack.push("To");
        assertEquals(1, stack.size());

        stack.push("be");
        assertEquals(2, stack.size());

        stack.pop();
        assertEquals("pop() should decrease stack size", 1, stack.size());
    }

    @Test
    public void testPeekKeepsStackSize() {
        stack.push("To");
        assertEquals(1, stack.size());

        stack.push("be");
        assertEquals(2, stack.size());

        stack.peek();
        assertEquals("pop() should keep stack size", 2, stack.size());
    }

    @Test
    public void testMixedPushPopPeek() {
        stack.push("To");
        assertEquals(1, stack.size());

        stack.push("be");
        assertEquals(2, stack.size());

        stack.push("or");
        assertEquals(3, stack.size());

        assertEquals("or", stack.peek());
        assertEquals(3, stack.size());

        assertEquals("or", stack.pop());
        assertEquals(2, stack.size());

        assertEquals("be", stack.peek());
        assertEquals(2, stack.size());

        assertEquals("be", stack.pop());
        assertEquals(1, stack.size());

        assertEquals("To", stack.peek());
        assertEquals(1, stack.size());

        stack.push("not");
        assertEquals(2, stack.size());

        assertEquals("not", stack.peek());
        assertEquals(2, stack.size());

        assertEquals("not", stack.pop());
        assertEquals(1, stack.size());
    }

    @Test(expected = PopEmptyStackException.class)
    public void testPopEmptyStackException() {
        stack.pop();
    }

    @Test(expected = PeekEmptyStackException.class)
    public void testPeekEmptyStackException() {
        stack.peek();
    }
}
