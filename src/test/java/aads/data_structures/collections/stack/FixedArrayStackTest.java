package aads.data_structures.collections.stack;

import aads.data_structures.collections.stack.api.FixedCapacityStack;
import aads.data_structures.collections.stack.exception.PushFullStackException;
import aads.data_structures.collections.stack.implementation.FixedArrayStack;
import org.junit.Test;

import static org.junit.Assert.*;

public class FixedArrayStackTest extends StackTestBase<FixedArrayStack<String>> {

    protected FixedArrayStack<String> createStackInstance() {
        int capacity = 10;
        return new FixedArrayStack<String>(capacity);
    }

    @Test
    public void testFullStack() {
        int capacity = 2;
        stack = new FixedArrayStack<String>(capacity);

        stack.push("To");
        assertEquals(1, stack.size());

        stack.push("be");
        assertEquals(2, stack.size());

        assertTrue("Stack should be full", stack.isFull());
        assertEquals("Stack capacity = 2", 2, stack.capacity());
        assertFalse("Stack shouldn't be empty", stack.isEmpty());
    }

    @Test
    public void testFixedCapacity() {
        FixedCapacityStack<String> stack1 = new FixedArrayStack<String>(-1);
        FixedCapacityStack<String> stack2 = new FixedArrayStack<String>(0);
        FixedCapacityStack<String> stack3 = new FixedArrayStack<String>(1);
        FixedCapacityStack<String> stack4 = new FixedArrayStack<String>(2);

        assertEquals(1, stack1.capacity());
        assertEquals(1, stack2.capacity());
        assertEquals(1, stack3.capacity());
        assertEquals(2, stack4.capacity());
    }

    @Test(expected = PushFullStackException.class)
    public void testPushFullStackException() {
        int capacity = 1;
        FixedCapacityStack<String> stack = new FixedArrayStack<String>(capacity);
        stack.push("To");
        stack.push("be");
    }
}
