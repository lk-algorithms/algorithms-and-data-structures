package aads.data_structures.collections.stack;

import aads.data_structures.collections.stack.implementation.LinkedListStack;

public class LinkedListStackTest extends StackTestBase<LinkedListStack<String>> {

    protected LinkedListStack<String> createStackInstance() {
        return new LinkedListStack<String>();
    }
}
