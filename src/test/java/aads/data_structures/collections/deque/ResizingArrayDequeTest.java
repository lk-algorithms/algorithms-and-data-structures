package aads.data_structures.collections.deque;

import aads.data_structures.collections.deque.implementation.ResizingArrayDeque;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResizingArrayDequeTest extends DequeTestBase<ResizingArrayDeque<String>> {

    protected ResizingArrayDeque<String> createDequeInstance() {
        return new ResizingArrayDeque<String>();
    }

    @Test
    public void testChangingCapacity() {
        assertEquals(0, deque.size());
        assertEquals(1, deque.capacity());

        deque.pushRight("To");
        assertEquals(1, deque.size());
        assertEquals(1, deque.capacity());

        deque.pushLeft("be");
        assertEquals(2, deque.size());
        assertEquals(2, deque.capacity());

        deque.pushRight("or");
        assertEquals(3, deque.size());
        assertEquals(4, deque.capacity());

        deque.pushLeft("not");
        assertEquals(4, deque.size());
        assertEquals(4, deque.capacity());

        deque.pushRight("to");
        assertEquals(5, deque.size());
        assertEquals(8, deque.capacity());

        deque.popRight();
        assertEquals(4, deque.size());
        assertEquals(8, deque.capacity());

        deque.popLeft();
        assertEquals(3, deque.size());
        assertEquals(8, deque.capacity());

        deque.popRight();
        assertEquals(2, deque.size());
        assertEquals(4, deque.capacity());

        deque.popLeft();
        assertEquals(1, deque.size());
        assertEquals(2, deque.capacity());

        deque.popRight();
        assertEquals(0, deque.size());
        assertEquals(2, deque.capacity());
    }
}
