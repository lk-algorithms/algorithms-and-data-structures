package aads.data_structures.collections.deque;

import aads.data_structures.collections.deque.api.Deque;
import aads.data_structures.collections.deque.exception.PopEmptyDequeException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class DequeTestBase<T extends Deque<String>> {

    protected T deque;

    protected abstract T createDequeInstance();

    @Before
    public void setUp() {
        deque = createDequeInstance();
    }

    @Test
    public void testInitiallyDequeIsEmpty() {
        assertTrue("Initially deque should be empty", deque.isEmpty());
        assertEquals("Initially deque has size = 0", 0, deque.size());
    }

    @Test
    public void testPushIncreasesDequeSize() {
        assertEquals(0, deque.size());

        deque.pushLeft("To");
        assertEquals("pushLeft() should increase deque size", 1, deque.size());

        deque.pushRight("be");
        assertEquals("pushRight() should increase deque size", 2, deque.size());
    }

    @Test
    public void testPopDecreasesDequeSize() {
        deque.pushRight("To");
        assertEquals(1, deque.size());

        deque.pushLeft("be");
        assertEquals(2, deque.size());

        deque.popLeft();
        assertEquals("popLeft() should decrease deque size", 1, deque.size());

        deque.popRight();
        assertEquals("popRight() should decrease deque size", 0, deque.size());
    }

    @Test
    public void testMixedPushPopLeft() {
        deque.pushLeft("To");
        assertEquals(1, deque.size());

        deque.pushLeft("be");
        assertEquals(2, deque.size());

        deque.pushLeft("or");
        assertEquals(3, deque.size());

        assertEquals("or", deque.popLeft());
        assertEquals(2, deque.size());

        assertEquals("be", deque.popLeft());
        assertEquals(1, deque.size());

        deque.pushLeft("not");
        assertEquals(2, deque.size());

        assertEquals("not", deque.popLeft());
        assertEquals(1, deque.size());

        assertEquals("To", deque.popLeft());
        assertEquals(0, deque.size());
    }

    @Test
    public void testMixedPushPopRight() {
        deque.pushRight("To");
        assertEquals(1, deque.size());

        deque.pushRight("be");
        assertEquals(2, deque.size());

        deque.pushRight("or");
        assertEquals(3, deque.size());

        assertEquals("or", deque.popRight());
        assertEquals(2, deque.size());

        assertEquals("be", deque.popRight());
        assertEquals(1, deque.size());

        deque.pushRight("not");
        assertEquals(2, deque.size());

        assertEquals("not", deque.popRight());
        assertEquals(1, deque.size());

        assertEquals("To", deque.popRight());
        assertEquals(0, deque.size());
    }

    @Test
    public void testMixedPushPop() {
        deque.pushRight("To");
        assertEquals(1, deque.size());

        deque.pushLeft("be");
        assertEquals(2, deque.size());

        deque.pushRight("or");
        assertEquals(3, deque.size());

        assertEquals("or", deque.popRight());
        assertEquals(2, deque.size());

        assertEquals("To", deque.popRight());
        assertEquals(1, deque.size());

        deque.pushLeft("not");
        assertEquals(2, deque.size());

        assertEquals("not", deque.popLeft());
        assertEquals(1, deque.size());

        assertEquals("be", deque.popLeft());
        assertEquals(0, deque.size());
    }

    @Test(expected = PopEmptyDequeException.class)
    public void testPopLeftEmptyDequeException() {
        deque.popLeft();
    }

    @Test(expected = PopEmptyDequeException.class)
    public void testPopRightEmptyDequeException() {
        deque.popRight();
    }

    // Test for iterator
}
