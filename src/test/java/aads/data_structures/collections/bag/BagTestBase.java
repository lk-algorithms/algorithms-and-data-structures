package aads.data_structures.collections.bag;

import aads.data_structures.collections.bag.api.Bag;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class BagTestBase<T extends Bag<String>> {

    protected T bag;

    protected abstract T createBagInstance();

    @Before
    public void setUp() {
        bag = createBagInstance();
    }

    @Test
    public void testInitiallyBagIsEmpty() {
        assertTrue("Initially bag should be empty", bag.isEmpty());
        assertEquals("Initially bag has size = 0", 0, bag.size());
    }

    @Test
    public void testAddIncreasesBagSize() {
        assertEquals(0, bag.size());
        bag.add("To");
        assertEquals("add() should increase bag size", 1, bag.size());
    }

    @Test
    public void testBagIterator() {
        List<String> expected = Arrays.asList("To", "be", "or", "not", "to", "be", "that",
                "is", "the", "question", "!");

        bag.add("To");
        bag.add("be");
        bag.add("or");
        bag.add("not");
        bag.add("to");
        bag.add("be");
        bag.add("that");
        bag.add("is");
        bag.add("the");
        bag.add("question");
        bag.add("!");

        assertEquals("Bag has expected size", expected.size(), bag.size());

        List<String> actual = new ArrayList<>();
        for (String s : bag) {
            actual.add(s);
        }

        assertTrue("Bag has expected items", expected.containsAll(actual));
    }
}
