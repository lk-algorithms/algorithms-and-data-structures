package aads.data_structures.collections.bag;

import aads.data_structures.collections.bag.implementation.RandomBag;

public class RandomBagTest extends BagTestBase<RandomBag<String>> {

    protected RandomBag<String> createBagInstance() {
        return new RandomBag<String>();
    }

}