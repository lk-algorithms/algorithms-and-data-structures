package aads.data_structures.collections.bag;

import aads.data_structures.collections.bag.implementation.LinkedListBag;

public class LinkedListBagTest extends BagTestBase<LinkedListBag<String>> {

    protected LinkedListBag<String> createBagInstance() {
        return new LinkedListBag<String>();
    }

}