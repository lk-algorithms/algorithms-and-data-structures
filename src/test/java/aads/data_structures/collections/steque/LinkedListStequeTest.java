package aads.data_structures.collections.steque;

import aads.data_structures.collections.steque.implementation.LinkedListSteque;

public class LinkedListStequeTest extends StequeTestBase<LinkedListSteque<String>> {

    protected LinkedListSteque<String> createStequeInstance() {
        return new LinkedListSteque<String>();
    }
}
