package aads.data_structures.collections.steque;

import aads.data_structures.collections.steque.api.Steque;
import aads.data_structures.collections.steque.exception.PeekEmptyStequeException;
import aads.data_structures.collections.steque.exception.PopEmptyStequeException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class StequeTestBase<T extends Steque<String>> {

    protected T steque;

    protected abstract T createStequeInstance();

    @Before
    public void setUp() {
        steque = createStequeInstance();
    }

    @Test
    public void testInitiallyStequeIsEmpty() {
        assertTrue("Initially steque should be empty", steque.isEmpty());
        assertEquals("Initially steque has size = 0", 0, steque.size());
    }

    @Test
    public void testPushIncreasesStequeSize() {
        assertEquals(0, steque.size());
        steque.push("To");
        assertEquals("push() should increase steque size", 1, steque.size());
    }

    @Test
    public void testEnqueueIncreasesStequeSize() {
        assertEquals(0, steque.size());
        steque.enqueue("To");
        assertEquals("enqueue() should increase steque size", 1, steque.size());
    }

    @Test
    public void testPopDecreasesStequeSize() {
        steque.push("To");
        assertEquals(1, steque.size());

        steque.push("be");
        assertEquals(2, steque.size());

        steque.pop();
        assertEquals("pop() should decrease steque size", 1, steque.size());
    }

    @Test
    public void testPeekKeepsStequeSize() {
        steque.push("To");
        assertEquals(1, steque.size());

        steque.push("be");
        assertEquals(2, steque.size());

        steque.peek();
        assertEquals("pop() should keep steque size", 2, steque.size());
    }

    @Test
    public void testMixedPushPopPeekEnqueue() {
        steque.push("To");
        assertEquals(1, steque.size());

        steque.enqueue("be");
        assertEquals(2, steque.size());

        steque.push("or");
        assertEquals(3, steque.size());

        assertEquals("or", steque.peek());
        assertEquals(3, steque.size());

        assertEquals("or", steque.pop());
        assertEquals(2, steque.size());

        assertEquals("To", steque.peek());
        assertEquals(2, steque.size());

        assertEquals("To", steque.pop());
        assertEquals(1, steque.size());

        assertEquals("be", steque.peek());
        assertEquals(1, steque.size());

        steque.enqueue("not");
        assertEquals(2, steque.size());

        assertEquals("be", steque.peek());
        assertEquals(2, steque.size());

        assertEquals("be", steque.pop());
        assertEquals(1, steque.size());
    }

    @Test(expected = PopEmptyStequeException.class)
    public void testPopEmptyStequeException() {
        steque.pop();
    }

    @Test(expected = PeekEmptyStequeException.class)
    public void testPeekEmptyStequeException() {
        steque.peek();
    }
}
