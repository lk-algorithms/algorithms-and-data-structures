package aads.algorithms.move_front.api;

/**
 * A well-known move-to-front strategy used for caching or data compression.
 */
public interface MoveFront {
    /**
     * Push character to the buffer
     *
     * @param ch character
     * @return True when cache-hit, otherwise False
     */
    boolean push(char ch);

    /**
     * Display buffer
     *
     * @return string representation of the buffer
     */
    String buffer();
}