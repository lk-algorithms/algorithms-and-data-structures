package aads.algorithms.move_front.implementation;

import aads.algorithms.move_front.api.MoveFront;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class MoveFrontImpl implements MoveFront {
    private Node start;
    private int size = 0;

    @Override
    public boolean push(char ch) {
        boolean removed = remove(ch);
        add(ch);
        return removed;
    }

    private boolean remove(char ch) {
        if (size < 1) {
            return false;
        }
        if (size == 1) {
            if (start.ch == ch) {
                start = null;
                size--;
                return true;
            } else {
                return false;
            }
        }

        Node current = start;
        while (current.next != null) {
            if (current.next.ch == ch) {
                current.next = current.next.next;
                size--;
                return true;
            }
            current = current.next;
        }
        return false;
    }

    private void add(char ch) {
        Node n = new Node();
        n.ch = ch;
        n.next = start;
        start = n;
        size++;
    }

    @Override
    public String buffer() {
        BufferIterator iter = new BufferIterator();
        StringBuilder sb = new StringBuilder();
        while (iter.hasNext()) {
            sb.append(" " + iter.next());
        }
        return sb.toString().trim();
    }

    static private class Node {
        char ch;
        Node next;
    }

    private class BufferIterator implements Iterator<Character> {
        private Node current = start;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Character next() {
            if (current == null) {
                throw new NoSuchElementException();
            }

            char item = current.ch;
            current = current.next;
            return item;
        }
    }

}