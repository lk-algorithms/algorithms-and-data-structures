package aads.algorithms.listing_files.api;

public interface ListingFiles {
    String list(String path);
}