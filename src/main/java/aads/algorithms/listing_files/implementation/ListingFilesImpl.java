package aads.algorithms.listing_files.implementation;

import aads.algorithms.listing_files.api.ListingFiles;
import aads.algorithms.listing_files.exception.ListDirectoryIsFileException;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;


public class ListingFilesImpl implements ListingFiles {

    @Override
    public String list(String path) {
        File rootDir = new File(path);
        File[] rootContents = getDirContents(rootDir);

        StringBuilder listBuilder = new StringBuilder();
        list(rootContents, listBuilder, 0);
        return listBuilder.toString().trim();
    }

    private void list(File[] contents, StringBuilder listBuilder, int indention) {
        Arrays.sort(contents);
        for (File item : contents) {
            addListItem(listBuilder, item, indention);
            if (item.isDirectory()) {
                File[] childDirContents = getDirContents(item);
                list(childDirContents, listBuilder, indention + 1);
            }
        }
    }

    private void addListItem(StringBuilder listBuilder, File item, int indention) {
        String absPath = item.getAbsolutePath();
        String[] literalizedPath = absPath.split(Pattern.quote(File.separator));
        String itemName = literalizedPath[literalizedPath.length - 1];
        String indentStr = String.join("", Collections.nCopies(indention * 4, " "));
        listBuilder.append(indentStr);
        listBuilder.append(itemName + "\n");
    }

    private File[] getDirContents(File dir) {
        File[] contents = dir.listFiles();
        if(contents == null) {
            throw new ListDirectoryIsFileException();
        }
        return contents;
    }

}