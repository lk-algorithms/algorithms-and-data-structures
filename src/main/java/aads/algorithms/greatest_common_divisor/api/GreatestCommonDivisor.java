package aads.algorithms.greatest_common_divisor.api;

public interface GreatestCommonDivisor {
    long gcd(long p, long q);
}