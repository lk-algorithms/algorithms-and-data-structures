package aads.algorithms.greatest_common_divisor.implementation;

import aads.algorithms.greatest_common_divisor.api.GreatestCommonDivisor;

public class GreatestCommonDivisorImpl implements GreatestCommonDivisor {

    @Override
    public long gcd(long p, long q) {
        if (q == 0) return p;
        long r = p % q;
        return gcd(q, r);
    }
}