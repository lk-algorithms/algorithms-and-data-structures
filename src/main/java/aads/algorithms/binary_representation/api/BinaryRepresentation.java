package aads.algorithms.binary_representation.api;

public interface BinaryRepresentation {
    String toBinaryRepresentation(Long decimal);
}