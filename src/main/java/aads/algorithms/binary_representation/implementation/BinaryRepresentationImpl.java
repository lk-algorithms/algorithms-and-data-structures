package aads.algorithms.binary_representation.implementation;

import aads.Utils;
import aads.algorithms.binary_representation.api.BinaryRepresentation;

import java.util.Deque;
import java.util.LinkedList;

public class BinaryRepresentationImpl implements BinaryRepresentation {
    public String toBinaryRepresentation(Long decimal) {
        if (decimal < 0) {
            throw new IllegalArgumentException("decimal must be greater than 0");
        }

        if (decimal == 0) {
            return "0";
        }

        Deque<Long> stack = new LinkedList<Long>();

        while (decimal > 0) {
            stack.push(decimal % 2);
            decimal /= 2;
        }

        return Utils.iterableToString(stack);
    }
}