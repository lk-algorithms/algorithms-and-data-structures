package aads.algorithms.histogram.api;

public interface Histogram {
    int[] histogram(int[] nums, int max);
}