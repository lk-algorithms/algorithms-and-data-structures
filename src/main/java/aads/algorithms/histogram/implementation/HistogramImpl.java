package aads.algorithms.histogram.implementation;

import aads.algorithms.histogram.api.Histogram;

public class HistogramImpl implements Histogram {
    @Override
    public int[] histogram(int[] nums, int max) {
        if (max < 0) {
            throw new IllegalArgumentException("max must be a positive number");
        }

        int[] hist = new int[max + 1];
        for (int num : nums) {
            if (num > max) {
                throw new RuntimeException(String.format("num: %d is greater than max: %d", num, max));
            }
            hist[num]++;
        }

        return hist;
    }
}