package aads.algorithms.josephus.api;

public interface Josephus {
    String josephus(int m, int n);
}