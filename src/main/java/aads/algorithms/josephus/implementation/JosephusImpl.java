package aads.algorithms.josephus.implementation;

import aads.algorithms.josephus.api.Josephus;
import aads.data_structures.collections.queue.api.Queue;
import aads.data_structures.collections.queue.implementation.CircularLinkedListQueue;


public class JosephusImpl implements Josephus {
    @Override
    public String josephus(int m, int n) {
        if (m < 1 || n < 0) {
            throw new IllegalArgumentException(String.format("Invalid arguments, m=%d n=%d", m, n));
        }
        Circle circle = new Circle(n);
        StringBuilder seqSb = new StringBuilder();
        while (!circle.empty()) {
            circle.move(m);
            int next = circle.remove();
            seqSb.append(String.format(" %d", next));
        }
        return seqSb.toString().trim();
    }

    static private class Circle {
        private Queue<Integer> queue;

        Circle(int n) {
            initializeCircle(n);
        }

        private void initializeCircle(int n) {
            queue = new CircularLinkedListQueue<>();
            for (int i = 0; i < n; i++) {
                queue.enqueue(i);
            }
        }

        boolean empty() {
            return queue.isEmpty();
        }

        void move(int m) {
            while (m > 1) {
                queue.enqueue(queue.dequeue());
                m--;
            }
        }

        int remove() {
            return queue.dequeue();
        }
    }
}