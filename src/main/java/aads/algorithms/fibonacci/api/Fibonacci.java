package aads.algorithms.fibonacci.api;

public interface Fibonacci {
    long fibonacci(int n);
}