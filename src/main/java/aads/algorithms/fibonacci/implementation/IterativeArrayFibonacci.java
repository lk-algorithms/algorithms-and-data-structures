package aads.algorithms.fibonacci.implementation;

import aads.algorithms.fibonacci.api.Fibonacci;

public class IterativeArrayFibonacci implements Fibonacci {

    @Override
    public long fibonacci(int n) {
        if (n < 2) return n;
        long[] sequence = new long[n + 1];
        sequence[0] = 0;
        sequence[1] = 1;
        for (int i = 2; i <= n; i++) {
            sequence[i] = sequence[i - 1] + sequence[i - 2];
        }
        return sequence[n];
    }
}