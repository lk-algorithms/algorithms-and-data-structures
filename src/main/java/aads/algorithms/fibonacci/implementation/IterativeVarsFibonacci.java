package aads.algorithms.fibonacci.implementation;

import aads.algorithms.fibonacci.api.Fibonacci;

public class IterativeVarsFibonacci implements Fibonacci {

    @Override
    public long fibonacci(int n) {
        if (n < 2) return n;
        long prevLast = 0, last = 1;
        for (int i = 2; i <= n; i++) {
            long fib = last + prevLast;
            prevLast = last;
            last = fib;
        }
        return last;
    }
}