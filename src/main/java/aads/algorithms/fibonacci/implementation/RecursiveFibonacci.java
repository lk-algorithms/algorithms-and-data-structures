package aads.algorithms.fibonacci.implementation;

import aads.algorithms.fibonacci.api.Fibonacci;

public class RecursiveFibonacci implements Fibonacci {

    @Override
    public long fibonacci(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}