package aads.algorithms.sort.implementation;

import aads.algorithms.sort.SortUtils;
import aads.algorithms.sort.api.Sort;

public class SelectionSort<T extends Comparable<T>> implements Sort<T> {

    @Override
    public void sort(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int min = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (SortUtils.less(arr[j], arr[min])) {
                    min = j;
                }
            }

            SortUtils.exchange(arr, i, min);
        }
    }
}