package aads.algorithms.sort.implementation;

import aads.algorithms.sort.SortUtils;
import aads.algorithms.sort.api.Sort;

public class ShellSort<T extends Comparable<T>> implements Sort<T> {

    @Override
    public void sort(T[] arr) {
        int n = arr.length;
        int h = 1;

        while (h < n / 3) {
            h = h * 3 + 1;
        }

        while (h >= 1) {
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && SortUtils.less(arr[j], arr[j - h]); j -= h) {
                    SortUtils.exchange(arr, j, j - h);
                }
            }
            h /= 3;
        }
    }
}