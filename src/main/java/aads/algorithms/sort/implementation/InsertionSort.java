package aads.algorithms.sort.implementation;

import aads.algorithms.sort.SortUtils;
import aads.algorithms.sort.api.Sort;

public class InsertionSort<T extends Comparable<T>> implements Sort<T> {

    @Override
    public void sort(T[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0 && SortUtils.less(arr[j], arr[j - 1]); j--) {
                SortUtils.exchange(arr, j, j - 1);
            }
        }
    }
}