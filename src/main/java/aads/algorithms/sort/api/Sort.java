package aads.algorithms.sort.api;

public interface Sort<T extends Comparable<T>> {
    void sort(T[] arr);
}