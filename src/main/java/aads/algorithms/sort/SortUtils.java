package aads.algorithms.sort;

public class SortUtils {

    public static <T extends Comparable<T>> boolean less(T c1, T c2) {
        return c1.compareTo(c2) < 0;
    }

    public static <T> void exchange(T[] arr, int i, int j) {
        T iEl = arr[i];
        arr[i] = arr[j];
        arr[j] = iEl;
    }
}
