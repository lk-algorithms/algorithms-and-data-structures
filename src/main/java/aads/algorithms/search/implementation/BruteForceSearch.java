package aads.algorithms.search.implementation;

import aads.algorithms.search.api.Search;

public class BruteForceSearch<T extends Comparable<T>> implements Search<T> {

    @Override
    public int indexOf(T[] arr, T key) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == key) return i;
        }
        return -1;
    }
}