package aads.algorithms.search.implementation;

import aads.algorithms.search.api.Search;

public class IterativeBinarySearch<T extends Comparable<T>> implements Search<T> {

    @Override
    public int indexOf(T[] arr, T key) {
        int low = 0;
        int high = arr.length - 1;

        while (low <= high) {
            int mid = low + ((high - low) / 2);
            T elem = arr[mid];
            if (key.compareTo(elem) < 0) {
                high = mid - 1;
            } else if (key.compareTo(elem) > 0) {
                low = mid + 1;
            } else {
                return mid;
            }
        }

        return -1;
    }
}