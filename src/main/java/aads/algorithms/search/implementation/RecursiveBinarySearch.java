package aads.algorithms.search.implementation;

import aads.algorithms.search.api.Search;

public class RecursiveBinarySearch<T extends Comparable<T>> implements Search<T> {

    @Override
    public int indexOf(T[] arr, T key) {
        int low = 0;
        int high = arr.length - 1;

        return indexOf(arr, key, low, high);
    }

    private int indexOf(T[] arr, T key, int low, int high) {
        if (low > high) return -1;
        int mid = low + ((high - low) / 2);
        T elem = arr[mid];
        if (key.compareTo(elem) < 0) {
            return indexOf(arr, key, low, mid - 1);
        } else if (key.compareTo(elem) > 0) {
            return indexOf(arr, key, mid + 1, high);
        } else {
            return mid;
        }
    }
}