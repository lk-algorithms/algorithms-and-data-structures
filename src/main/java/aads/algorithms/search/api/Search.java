package aads.algorithms.search.api;

public interface Search<T extends Comparable<T>> {
    int indexOf(T[] arr, T key);
}