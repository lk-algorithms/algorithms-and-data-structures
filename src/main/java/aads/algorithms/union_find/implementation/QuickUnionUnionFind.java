package aads.algorithms.union_find.implementation;

public class QuickUnionUnionFind extends UnionFindAbs {

    public QuickUnionUnionFind(int n) {
        super(n);
    }

    /**
     * This implementation does:
     * - 2 accesses for finding components of p and q
     * - from 0 to 1 accesses when changing root for site q
     * -> 2 + 0 = 2 at least in total
     * -> 2 + 1 = 3 at most in total
     */
    @Override
    public void union(int p, int q) {
        int pId = find(p);
        int qId = find(q);
        if (pId == qId) return;
        id[q] = p;
        count--;
    }

    /**
     * This implementation does:
     * - when p is root -> 1 access at least in total
     * - when p needs to traverse all sites -> n accesses to check and n - 1 to modify
     * p, 2n - 1 at most in total
     */
    @Override
    public int find(int p) {
        while (p != id[p]) p = id[p];
        return p;
    }
}
