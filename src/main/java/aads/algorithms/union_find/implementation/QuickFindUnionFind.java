package aads.algorithms.union_find.implementation;

public class QuickFindUnionFind extends UnionFindAbs {

    public QuickFindUnionFind(int n) {
        super(n);
    }

    /**
     * This implementation does:
     * - 2 accesses for finding components of p and q
     * - n accesses when checking each entry of array id
     * - from 1 to n-1 accesses when changing entries of array id
     * -> 2 + n + 1 = n + 3 at least in total
     * -> 2 + n + n - 1 = 2n + 1 at most in total
     */
    @Override
    public void union(int p, int q) {
        int pId = find(p);
        int qId = find(q);
        if (pId == qId) return;

        for (int i = 0; i < id.length; i++) {
            if (id[i] == qId) {
                id[i] = pId;
            }
        }
        count--;
    }

    /**
     * This implementation does constant (exactly one) array accesses
     */
    @Override
    public int find(int p) {
        return id[p];
    }
}
