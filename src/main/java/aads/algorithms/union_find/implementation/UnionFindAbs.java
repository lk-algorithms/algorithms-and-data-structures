package aads.algorithms.union_find.implementation;

import aads.algorithms.union_find.api.UnionFind;
import aads.algorithms.union_find.exception.LessThanOneSiteException;

public abstract class UnionFindAbs implements UnionFind {
    int id[];
    int count;

    public UnionFindAbs(int n) {
        if (n < 1) {
            throw new LessThanOneSiteException();
        }
        count = n;
        id = new int[n];
        for (int i = 0; i < id.length; i++) {
            id[i] = i;
        }
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public int count() {
        return count;
    }
}
