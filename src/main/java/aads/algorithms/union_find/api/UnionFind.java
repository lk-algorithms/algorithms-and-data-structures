package aads.algorithms.union_find.api;

public interface UnionFind {
    /**
     * Connects two sites; ignores when sites already connected
     *
     * @param p site one
     * @param q site two
     */
    void union(int p, int q);

    /**
     * @param p a site
     * @return a component that the site belongs to
     */
    int find(int p);

    /**
     * @param p site one
     * @param q site two
     * @return whether the sites are connected
     */
    boolean connected(int p, int q);

    /**
     * @return number of components
     */
    int count();
}
