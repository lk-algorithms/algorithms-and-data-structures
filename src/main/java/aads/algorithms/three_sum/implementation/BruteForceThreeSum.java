package aads.algorithms.three_sum.implementation;

import aads.algorithms.three_sum.api.ThreeSum;


public class BruteForceThreeSum implements ThreeSum {

    @Override
    public int count(int[] arr) {
        int n = arr.length;
        int count = 0;
        for(int i = 0; i < n; i++) {
            for(int j = i + 1; j < n; j++) {
                for(int k = j + 1; k < n; k++) {
                    int sum = arr[i] + arr[j] + arr[k];
                    if(sum == 0) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
}