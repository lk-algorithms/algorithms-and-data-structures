package aads.algorithms.three_sum.api;

public interface ThreeSum {

    /**
     * Counts triples that sums to 0. It assumes arr to have distinct integers.
     * @param arr array of numbers to generate triples from.
     * @return count of triples summing to 0.
     */
    int count(int[] arr);
}