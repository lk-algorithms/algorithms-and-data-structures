package aads.algorithms.notation_converter.implementation;

import aads.algorithms.notation_converter.api.NotationConverter;

import java.util.Deque;
import java.util.LinkedList;

public class InfixToSuffixConverter implements NotationConverter {
    public String convert(String expression) {
        Deque<String> ops = new LinkedList<String>();
        Deque<String> exprs = new LinkedList<String>();

        for (Character ch : expression.toCharArray()) {
            if (ch == '(')
                ;
            else if (isOperation(ch))
                ops.push(ch.toString());
            else if (ch == ')') {
                String op = ops.pop();
                String expr1 = exprs.pop();
                String expr2 = exprs.pop();
                String newExpr = expr2 + expr1 + op;
                exprs.push(newExpr);
            } else
                exprs.push(ch.toString());
        }

        return exprs.pop();
    }

    private boolean isOperation(Character op) {
        return op == '+' || op == '-' || op == '*' || op == '/';
    }
}