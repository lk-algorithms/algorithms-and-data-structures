package aads.algorithms.notation_converter.api;

public interface NotationConverter {
    String convert(String expression);
}