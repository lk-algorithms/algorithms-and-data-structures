package aads.experiments.stopwatch.implementation;

import aads.experiments.stopwatch.api.Stopwatch;
import aads.experiments.stopwatch.exception.StopwatchMultipleInitializationException;

import java.time.Clock;

public class StopwatchImpl implements Stopwatch {

    private final Clock clock;
    private long start = -1;

    public StopwatchImpl(Clock clock) {
        if (clock == null) {
            throw new IllegalArgumentException("Clock must be not null");
        }
        this.clock = clock;
    }

    @Override
    public void start() {
        if (start > 0) {
            throw new StopwatchMultipleInitializationException();
        }
        start = clock.millis();
    }

    @Override
    public double elapsedTime() {
        long now = clock.millis();
        return (now - start) / 1000.0;
    }
}
