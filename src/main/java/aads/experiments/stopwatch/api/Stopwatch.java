package aads.experiments.stopwatch.api;

public interface Stopwatch {
    /**
     * Start measuring time.
     */
    void start();

    /**
     * Time (sec) passed since measurement started.
     *
     * @return seconds elapsed
     */
    double elapsedTime();
}
