package aads;

public final class Utils {

    private Utils() {
    }

    static public <T> String iterableToString(Iterable<T> iterable) {
        StringBuilder sb = new StringBuilder();
        for (T elem : iterable) {
            sb.append(elem);
        }
        return sb.toString();
    }

}