package aads.data_structures.collections.steque.api;

public interface Steque<Item> {
    void push(Item item);

    void enqueue(Item item);

    Item pop();

    Item peek();

    boolean isEmpty();

    int size();
}