package aads.data_structures.collections.steque.implementation;

import aads.data_structures.collections.steque.api.Steque;
import aads.data_structures.collections.steque.exception.PeekEmptyStequeException;
import aads.data_structures.collections.steque.exception.PopEmptyStequeException;

public class LinkedListSteque<Item> implements Steque<Item> {
    private Node first;
    private Node last;
    private int size;

    public void push(Item item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;

        if (isEmpty())
            last = first;

        size++;
    }

    public void enqueue(Item item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;

        if (isEmpty())
            first = last;
        else
            oldLast.next = last;
        size++;
    }

    public Item pop() {
        if (isEmpty())
            throw new PopEmptyStequeException();

        Item item = first.item;
        first = first.next;
        size--;

        if (isEmpty())
            last = null;

        return item;
    }

    public Item peek() {
        if (isEmpty())
            throw new PeekEmptyStequeException();

        return first.item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    private class Node {
        Item item;
        Node next;
    }
}
