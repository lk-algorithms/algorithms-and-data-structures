package aads.data_structures.collections.deque.implementation;

import aads.data_structures.collections.deque.api.CapacityDeque;
import aads.data_structures.collections.deque.exception.PopEmptyDequeException;

import java.util.Iterator;

public class ResizingArrayDeque<Item> implements CapacityDeque<Item> {

    private Item[] deque;
    private int size;
    private int start;
    private int end;

    public ResizingArrayDeque() {
        this.deque = (Item[]) new Object[1];
        this.size = 0;
        this.start = 0;
        this.end = 0;
    }

    private void resizeDeque(int newCapacity) {
        if (newCapacity < size()) {
            throw new RuntimeException("resizeDeque: newCapacity < size");
        }

        Item[] newDeque = (Item[]) new Object[newCapacity];
        for (int i = 0; i < size(); i++) {
            newDeque[i] = deque[(start + i) % capacity()];
        }
        start = 0;
        end = size() - 1;
        deque = newDeque;
    }

    public void pushLeft(Item s) {
        if (size() == capacity()) {
            resizeDeque(capacity() * 2);
        }
        start = Math.floorMod(start - 1, capacity());
        deque[start] = s;
        size++;
    }

    public void pushRight(Item s) {
        if (size() == capacity()) {
            resizeDeque(capacity() * 2);
        }
        end = (end + 1) % capacity();
        deque[end] = s;
        size++;
    }

    public Item popLeft() {
        if (isEmpty()) {
            throw new PopEmptyDequeException();
        }

        Item item = deque[start];
        start = (start + 1) % capacity();
        size--;

        if (4 * size == capacity()) {
            resizeDeque(capacity() / 2);
        }

        return item;
    }

    public Item popRight() {
        if (isEmpty()) {
            throw new PopEmptyDequeException();
        }

        Item item = deque[end];
        end = (end - 1) % capacity();
        size--;

        if (4 * size == capacity()) {
            resizeDeque(capacity() / 2);
        }

        return item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public int capacity() {
        return deque.length;
    }

    public Iterator<Item> iterator() {
        return new ArrayIterator();
    }

    private class ArrayIterator implements Iterator<Item> {
        private int current = start;
        private int steps = 0;

        public boolean hasNext() {
            return steps < size();
        }

        public void remove() {
        }

        public Item next() {
            Item it = deque[current];
            current = (current + 1) % capacity();
            steps++;
            return it;
        }
    }
}
