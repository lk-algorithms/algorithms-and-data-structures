package aads.data_structures.collections.deque.api;

public interface CapacityDeque<Item> extends Deque<Item> {
    int capacity();
}