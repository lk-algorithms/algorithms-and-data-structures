package aads.data_structures.collections.bag.api;

public interface Bag<Item> extends Iterable<Item> {
    void add(Item item);

    boolean isEmpty();

    int size();
}