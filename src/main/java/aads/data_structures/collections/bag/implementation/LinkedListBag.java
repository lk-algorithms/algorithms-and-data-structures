package aads.data_structures.collections.bag.implementation;

import aads.data_structures.collections.bag.api.Bag;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedListBag<Item> implements Bag<Item> {
    private Node first;
    private int size;

    public void add(Item item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        size++;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public Iterator<Item> iterator() {
        return new LinkedListIterator();
    }

    private class Node {
        Item item;
        Node next;
    }

    private class LinkedListIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
        }

        public Item next() {
            if (current == null) {
                throw new NoSuchElementException();
            }

            Item item = current.item;
            current = current.next;
            return item;
        }
    }
}
