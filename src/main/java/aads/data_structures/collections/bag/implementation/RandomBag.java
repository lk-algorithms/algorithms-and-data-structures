package aads.data_structures.collections.bag.implementation;

import aads.data_structures.collections.bag.api.Bag;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomBag<Item> implements Bag<Item> {
    private Item[] bag;
    private int idx;
    private int size;

    public RandomBag() {
        this.bag = (Item[]) new Object[1];
        this.idx = 0;
        this.size = 0;
    }

    private void resizeBag(int newCapacity) {
        if (newCapacity < size()) {
            throw new RuntimeException("resizeBag: newCapacity < size");
        }

        Item[] newBag = (Item[]) new Object[newCapacity];
        for (int i = 0; i < size(); i++) {
            newBag[i] = bag[i];
        }
        bag = newBag;
    }

    public void add(Item item) {
        if (size() == capacity()) {
            resizeBag(2 * capacity());
        }

        bag[idx++] = item;
        size++;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public Iterator<Item> iterator() {
        return new RandomOrderArrayIterator();
    }

    private int capacity() {
        return bag.length;
    }

    private class RandomOrderArrayIterator implements Iterator<Item> {
        private int current;
        private List<Integer> order;

        public RandomOrderArrayIterator() {
            current = 0;
            order = IntStream.range(0, size()).boxed().collect(Collectors.toList());
            Collections.shuffle(order);
        }

        public boolean hasNext() {
            return current < size();
        }

        public void remove() {
        }

        public Item next() {
            if (current > size()) {
                throw new NoSuchElementException();
            }

            Item item = bag[order.get(current++)];
            return item;
        }
    }
}
