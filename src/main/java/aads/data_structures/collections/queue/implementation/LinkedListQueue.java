package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.Queue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;

public class LinkedListQueue<Item> implements Queue<Item> {
    protected Node first;
    protected Node last;
    protected int size;

    public void enqueue(Item item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;

        if (isEmpty())
            first = last;
        else
            oldLast.next = last;
        size++;
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new QueueDeleteOutOfBoundsException();
        }

        Item item = first.item;
        first = first.next;
        size--;
        if (isEmpty())
            last = null;
        return item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    protected class Node {
        Item item;
        Node next;
    }
}
