package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.Queue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;

public class CircularLinkedListQueue<Item> implements Queue<Item> {
    private Node last;
    private int size;

    public void enqueue(Item item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;

        if (isEmpty())
            last.next = last;
        else {
            last.next = oldLast.next;
            oldLast.next = last;
        }
        size++;
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new QueueDeleteOutOfBoundsException();
        }

        Node first = last.next;
        Item item = first.item;
        last.next = first.next;
        size--;
        if (isEmpty())
            last = null;
        return item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    private class Node {
        Item item;
        Node next;
    }
}
