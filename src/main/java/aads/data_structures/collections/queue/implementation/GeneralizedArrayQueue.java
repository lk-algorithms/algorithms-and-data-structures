package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.GeneralizedQueue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;

public class GeneralizedArrayQueue<Item> extends ResizingArrayQueue<Item> implements GeneralizedQueue<Item> {
    @Override
    public Item delete(int k) {
        if (k < 0 || k >= size) {
            throw new QueueDeleteOutOfBoundsException();
        }

        Item item = queue[start + k];

        for(int i = start + k; i > start; i--) {
            queue[i] = queue[i-1];
        }

        start = (start + 1) % capacity();
        size--;

        if (4 * size == capacity()) {
            resizeQueue(capacity() / 2);
        }

        return item;
    }


}
