package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.CapacityQueue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;

public class ResizingArrayQueue<Item> implements CapacityQueue<Item> {

    protected Item[] queue;
    protected int size;
    protected int start;
    protected int end;

    public ResizingArrayQueue() {
        this.queue = (Item[]) new Object[1];
        this.size = 0;
        this.start = 0;
        this.end = 0;
    }

    protected void resizeQueue(int newCapacity) {
        Item[] newQueue = (Item[]) new Object[newCapacity];
        for (int i = 0; i < size(); i++) {
            newQueue[i] = queue[(start + i) % capacity()];
        }
        start = 0;
        end = size();
        queue = newQueue;
    }

    public void enqueue(Item s) {
        if (size() == capacity()) {
            resizeQueue(capacity() * 2);
        }
        queue[end] = s;
        end = (end + 1) % capacity();
        size++;
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new QueueDeleteOutOfBoundsException();
        }

        Item item = queue[start];
        start = (start + 1) % capacity();
        size--;

        if (4 * size == capacity()) {
            resizeQueue(capacity() / 2);
        }

        return item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public int capacity() {
        return queue.length;
    }
}
