package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.GeneralizedQueue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;

public class GeneralizedLinkedListQueue<Item> extends LinkedListQueue<Item> implements GeneralizedQueue<Item> {
    @Override
    public Item delete(int k) {
        if(k < 0 || k >= size) {
            throw new QueueDeleteOutOfBoundsException();
        }

        if(k == 0) {
            return dequeue();
        }

        Node iter = first;
        for(int i = 0; i < k - 1; i++) {
            iter = iter.next;
        }
        Node del = iter.next;
        iter.next = del.next;
        size--;
        if (isEmpty())
            last = null;
        return del.item;
    }
}
