package aads.data_structures.collections.queue.implementation;

import aads.data_structures.collections.queue.api.FixedCapacityQueue;
import aads.data_structures.collections.queue.exception.QueueDeleteOutOfBoundsException;
import aads.data_structures.collections.queue.exception.EnqueueFullQueueException;

public class FixedArrayQueue<Item> implements FixedCapacityQueue<Item> {

    private Item[] queue;
    private int size;
    private int start;
    private int end;

    public FixedArrayQueue(int capacity) {
        // Minimum capacity of the queue is 1
        if (capacity < 1) {
            capacity = 1;
        }
        this.queue = (Item[]) new Object[capacity];
        this.size = 0;
        this.start = 0;
        this.end = 0;
    }

    public void enqueue(Item s) {
        if (!isFull()) {
            queue[end] = s;
            end = (end + 1) % queue.length;
            size++;
        } else {
            throw new EnqueueFullQueueException();
        }
    }

    public Item dequeue() {
        if (!isEmpty()) {
            Item item = queue[start];
            start = (start + 1) % queue.length;
            size--;
            return item;
        } else {
            throw new QueueDeleteOutOfBoundsException();
        }
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public int capacity() {
        return queue.length;
    }

    public boolean isFull() {
        return size() == capacity();
    }

}
