package aads.data_structures.collections.queue.api;

public interface GeneralizedQueue<Item> extends Queue<Item> {
    Item delete(int k);
}
