package aads.data_structures.collections.queue.api;

public interface CapacityQueue<Item> extends Queue<Item> {
    int capacity();
}