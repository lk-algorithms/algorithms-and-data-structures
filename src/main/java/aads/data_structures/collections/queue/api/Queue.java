package aads.data_structures.collections.queue.api;

public interface Queue<Item> {
    void enqueue(Item item);

    Item dequeue();

    boolean isEmpty();

    int size();
}