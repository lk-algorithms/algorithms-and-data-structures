package aads.data_structures.collections.queue.api;

public interface FixedCapacityQueue<Item> extends CapacityQueue<Item> {
    boolean isFull();
}