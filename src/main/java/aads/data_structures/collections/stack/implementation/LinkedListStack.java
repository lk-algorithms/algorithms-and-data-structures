package aads.data_structures.collections.stack.implementation;

import aads.data_structures.collections.stack.api.Stack;
import aads.data_structures.collections.stack.exception.PeekEmptyStackException;
import aads.data_structures.collections.stack.exception.PopEmptyStackException;

public class LinkedListStack<Item> implements Stack<Item> {
    Node first;
    int size;

    public void push(Item item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        size++;
    }

    public Item pop() {
        if (isEmpty())
            throw new PopEmptyStackException();

        Item item = first.item;
        first = first.next;
        size--;
        return item;
    }

    public Item peek() {
        if (isEmpty())
            throw new PeekEmptyStackException();

        return first.item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    private class Node {
        Item item;
        Node next;
    }
}
