package aads.data_structures.collections.stack.implementation;

import aads.data_structures.collections.stack.api.CapacityStack;
import aads.data_structures.collections.stack.exception.PeekEmptyStackException;
import aads.data_structures.collections.stack.exception.PopEmptyStackException;

public class ResizingArrayStack<Item> implements CapacityStack<Item> {

    private Item[] stack;
    private int n;

    public ResizingArrayStack() {
        this.stack = (Item[]) new Object[1];
        this.n = 0;
    }

    private void resizeStack(int capacity) {
        Item[] newStack = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++) {
            newStack[i] = stack[i];
        }
        stack = newStack;
    }

    public void push(Item s) {
        if (n == stack.length) {
            resizeStack(stack.length * 2);
        }
        stack[n++] = s;
    }

    public Item pop() {
        if (isEmpty()) {
            throw new PopEmptyStackException();
        }

        Item item = stack[--n];

        if (4 * n == stack.length) {
            resizeStack(stack.length / 2);
        }

        return item;
    }

    public Item peek() {
        if (!isEmpty()) {
            return stack[n - 1];
        } else {
            throw new PeekEmptyStackException();
        }
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public int capacity() {
        return stack.length;
    }

}
