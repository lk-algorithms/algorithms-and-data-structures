package aads.data_structures.collections.stack.implementation;

import aads.data_structures.collections.stack.api.FixedCapacityStack;
import aads.data_structures.collections.stack.exception.PeekEmptyStackException;
import aads.data_structures.collections.stack.exception.PopEmptyStackException;
import aads.data_structures.collections.stack.exception.PushFullStackException;

public class FixedArrayStack<Item> implements FixedCapacityStack<Item> {

    private Item[] stack;
    private int n;

    public FixedArrayStack(int capacity) {
        // Minimum capacity of the stack is 1
        if (capacity < 1) {
            capacity = 1;
        }
        this.stack = (Item[]) new Object[capacity];
        this.n = 0;
    }

    public void push(Item s) {
        if (!isFull()) {
            stack[n++] = s;
        } else {
            throw new PushFullStackException();
        }
    }

    public Item pop() {
        if (!isEmpty()) {
            return stack[--n];
        } else {
            throw new PopEmptyStackException();
        }
    }

    public Item peek() {
        if (!isEmpty()) {
            return stack[n - 1];
        } else {
            throw new PeekEmptyStackException();
        }
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public int capacity() {
        return stack.length;
    }

    public boolean isFull() {
        return n == capacity();
    }

}
