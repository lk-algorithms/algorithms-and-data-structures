package aads.data_structures.collections.stack.api;

public interface CapacityStack<Item> extends Stack<Item> {
    int capacity();
}