package aads.data_structures.collections.stack.api;

public interface FixedCapacityStack<Item> extends CapacityStack<Item> {
    boolean isFull();
}