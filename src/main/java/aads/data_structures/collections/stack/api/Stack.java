package aads.data_structures.collections.stack.api;

public interface Stack<Item> {
    void push(Item item);

    Item pop();

    Item peek();

    boolean isEmpty();

    int size();
}