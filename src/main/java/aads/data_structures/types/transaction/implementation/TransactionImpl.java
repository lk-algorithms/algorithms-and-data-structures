package aads.data_structures.types.transaction.implementation;

import aads.data_structures.types.date.api.Date;
import aads.data_structures.types.date.implementation.MDYDate;
import aads.data_structures.types.transaction.api.Transaction;
import aads.data_structures.types.transaction.exception.TransactionParsingException;

import java.util.Objects;

public class TransactionImpl implements Transaction {

    private String who;
    private Date when;
    private double amount;

    public TransactionImpl(String who, Date when, double amount) {
        this.who = who;
        this.when = when;
        this.amount = amount;
    }

    public TransactionImpl(String transaction) {
        String[] parts = transaction.split(" ");
        if (parts.length != 3) {
            throw new TransactionParsingException(String.format("%s in not a valid transaction", transaction));
        }

        try {
            this.who = parts[0];
            this.when = new MDYDate(parts[1]);
            this.amount = Double.parseDouble(parts[2]);
        } catch (Exception ex) {
            throw new TransactionParsingException(String.format("%s in not a valid transaction", transaction));
        }
    }

    @Override
    public String who() {
        return who;
    }

    @Override
    public Date when() {
        return when;
    }

    @Override
    public double amount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;
        TransactionImpl other = (TransactionImpl) o;
        return this.who.equals(other.who)
                && this.when.equals(other.when)
                && this.amount == other.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(who, when, amount);
    }

    @Override
    public int compareTo(Transaction other) {
        if (other == null) throw new RuntimeException("compareTo(null)");
        if (other == this) return 0;

        int compareWhen = when.compareTo(other.when());
        if (compareWhen == 0) {
            int compareWho = who.compareTo(other.who());
            if (compareWho == 0) {
                return Double.compare(amount, other.amount());
            }
            return compareWho;
        }
        return compareWhen;
    }

    @Override
    public String toString() {
        return String.format("%s %s %.2f", who, when, amount);
    }
}
