package aads.data_structures.types.transaction.api;

import aads.data_structures.types.date.api.Date;

public interface Transaction extends Comparable<Transaction> {
    String who();

    Date when();

    double amount();
}
