package aads.data_structures.types.transaction.exception;

public class TransactionParsingException extends RuntimeException {

    public TransactionParsingException(String message) {
        super(message);
    }

}
