package aads.data_structures.types.date.implementation;

import aads.data_structures.types.date.api.Date;
import aads.data_structures.types.date.exception.DateBeforeEpochStartException;
import aads.data_structures.types.date.exception.DateParsingException;
import aads.data_structures.types.date.exception.DayOutOfBoundsException;
import aads.data_structures.types.date.exception.MonthOutOfBoundsException;

import java.util.Objects;

public class MDYDate implements Date {

    private int month;
    private int day;
    private int year;

    public MDYDate(int month, int day, int year) {
        validate(month, day, year);
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public MDYDate(String date) {
        int[] dateParts = parse(date);
        validate(dateParts[0], dateParts[1], dateParts[2]);
        this.month = dateParts[0];
        this.day = dateParts[1];
        this.year = dateParts[2];
    }

    private int[] parse(String date) {
        String[] dateParts = date.split("/");

        if (dateParts.length != 3) {
            throw new DateParsingException(String.format("'%s' is invalid date", date));
        }

        try {
            return new int[]{
                    Integer.parseInt(dateParts[0]),
                    Integer.parseInt(dateParts[1]),
                    Integer.parseInt(dateParts[2])
            };
        } catch (Exception ex) {
            throw new DateParsingException(String.format("'%s' is invalid date", date));
        }
    }

    private void validate(int month, int day, int year) {
        if (year < 1970) throw new DateBeforeEpochStartException("Year must be equal to or greater than 1970");
        if (month < 1 || month > 12) throw new MonthOutOfBoundsException("Month must be between 1 and 12");

        int daysInMonth = Month.getMonth(month).getDays();
        daysInMonth += getFebruaryModification(month, year);

        if (day < 1 || day > daysInMonth)
            throw new DayOutOfBoundsException("Day in this month of this year must be between 1 and " + daysInMonth);
    }

    private int getFebruaryModification(int month, int year) {
        if (Month.getMonth(month) == Month.February && isLeapYear(year)) {
            return 1;
        }
        return 0;
    }

    private boolean isLeapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    @Override
    public String toString() {
        return String.format("%s/%s/%s", month, day, year);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof MDYDate)) return false;
        MDYDate other = (MDYDate) o;
        return this.month == other.month &&
                this.day == other.day &&
                this.year == other.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(month, day, year);
    }

    @Override
    public int month() {
        return month;
    }

    @Override
    public int day() {
        return day;
    }

    @Override
    public int year() {
        return year;
    }

    @Override
    public String dayOfWeek() {
        // 1/1/1970 is Thursday
        DOW epochDOW = DOW.Thursday;
        int daysFromEpoch = getDaysFromEpoch();
        int diffDOW = daysFromEpoch % 7;
        DOW dateDOW = DOW.valueOf((epochDOW.ordinal() + diffDOW) % 7);
        return dateDOW.name();
    }

    private int getDaysFromEpoch() {
        int countMonth = 1;
        int countYear = 1970;
        int counter = 0;

        while (countYear < this.year) {
            counter += 365 + getFebruaryModification(2, countYear);
            countYear++;
        }

        while (countMonth < this.month) {
            counter += Month.getMonth(countMonth).getDays() + getFebruaryModification(countMonth, countYear);
            countMonth++;
        }

        counter += this.day - 1;

        return counter;
    }

    @Override
    public int compareTo(Date date) {
        if (date == null) throw new RuntimeException("Compare date to null");
        if (this.equals(date)) return 0;
        if (this.year < date.year() ||
                (this.year == date.year() && this.month < date.month()) ||
                (this.year == date.year() && this.month == date.month() && this.day < date.day())) {
            return -1;
        }
        return 1;
    }

    private enum Month {
        January(31),
        February(28),
        March(31),
        April(30),
        May(31),
        June(30),
        July(31),
        August(31),
        September(30),
        October(31),
        November(30),
        December(31);

        private final int monthDays;

        Month(int monthDays) {
            this.monthDays = monthDays;
        }

        static Month getMonth(int month) {
            return Month.values()[month - 1];
        }

        int getDays() {
            return monthDays;
        }
    }

    private enum DOW {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday;

        static DOW valueOf(int i) {
            return values()[i];
        }
    }
}
