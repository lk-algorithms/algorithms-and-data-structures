package aads.data_structures.types.date.exception;

public class DateParsingException extends RuntimeException {

    public DateParsingException(String message) {
        super(message);
    }

}
