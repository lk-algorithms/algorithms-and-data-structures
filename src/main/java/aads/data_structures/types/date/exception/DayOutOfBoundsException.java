package aads.data_structures.types.date.exception;

public class DayOutOfBoundsException extends InvalidDateException {

    public DayOutOfBoundsException(String message) {
        super(message);
    }

}
