package aads.data_structures.types.date.exception;

public class DateBeforeEpochStartException extends InvalidDateException {

    public DateBeforeEpochStartException(String message) {
        super(message);
    }

}
