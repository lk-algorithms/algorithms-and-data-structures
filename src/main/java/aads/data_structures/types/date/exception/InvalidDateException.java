package aads.data_structures.types.date.exception;

public abstract class InvalidDateException extends RuntimeException {

    public InvalidDateException(String message) {
        super(message);
    }

}
