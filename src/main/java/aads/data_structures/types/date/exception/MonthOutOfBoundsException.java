package aads.data_structures.types.date.exception;

public class MonthOutOfBoundsException extends InvalidDateException {

    public MonthOutOfBoundsException(String message) {
        super(message);
    }

}
