package aads.data_structures.types.date.api;

public interface Date extends Comparable<Date> {
    int month();

    int day();

    int year();

    String dayOfWeek();
}
