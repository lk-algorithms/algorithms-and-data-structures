package aads.data_structures.types.counter.implementation;

import aads.data_structures.types.counter.api.Counter;

public class CounterImpl implements Counter {

    int counter = 0;

    @Override
    public void increment() {
        counter++;
    }

    @Override
    public int tally() {
        return counter;
    }

    @Override
    public String toString() {
        return "Counter: " + counter;
    }
}
