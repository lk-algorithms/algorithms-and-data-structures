package aads.data_structures.types.counter.api;

public interface Counter {
    void increment();

    int tally();
}
