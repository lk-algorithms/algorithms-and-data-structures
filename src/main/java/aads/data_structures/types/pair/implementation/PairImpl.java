package aads.data_structures.types.pair.implementation;

import aads.data_structures.types.pair.api.Pair;

import java.util.Objects;

public class PairImpl<T1, T2> implements Pair<T1, T2> {

    private final T1 first;
    private final T2 second;

    public PairImpl(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public T1 first() {
        return first;
    }

    @Override
    public T2 second() {
        return second;
    }

    @Override
    public String toString() {
        return String.format("<%s, %s>", first, second);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof PairImpl)) return false;
        PairImpl other = (PairImpl) o;
        return first.equals(other.first) && second.equals(other.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
