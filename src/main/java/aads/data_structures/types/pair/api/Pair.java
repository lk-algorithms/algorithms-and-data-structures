package aads.data_structures.types.pair.api;

public interface Pair<T1, T2> {
    T1 first();
    T2 second();
}
