package aads.data_structures.types.matrix.implementation;

import aads.data_structures.types.matrix.api.Matrix;
import aads.data_structures.types.matrix.exception.ArrayNotMatrixShapeException;
import aads.data_structures.types.matrix.exception.NotMatchingMatricesMultiplicationException;

import java.util.Arrays;

public class ArrayMatrix implements Matrix {

    private double[][] matrix;
    private int rows;
    private int cols;

    public ArrayMatrix(double[][] array2d) {
        validateShape(array2d);
        initializeMatrix(array2d);
        setDimensions();
    }

    private void setDimensions() {
        rows = matrix.length;
        if (rows == 0) {
            cols = 0;
        } else {
            cols = matrix[0].length;
        }
    }

    private void initializeMatrix(double[][] array2d) {
        matrix = new double[array2d.length][];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = array2d[i].clone();
        }
    }

    private void validateShape(double[][] array2d) {
        int[] lengths = new int[array2d.length];
        for (int i = 0; i < array2d.length; i++) {
            lengths[i] = array2d[i].length;
        }

        if (lengths.length > 0) {
            int firstLength = lengths[0];
            boolean valid = firstLength != 0;
            for (int i = 1; i < lengths.length && valid; i++) {
                if (firstLength != lengths[i]) {
                    valid = false;
                }
            }
            if (!valid) {
                throw new ArrayNotMatrixShapeException();
            }
        }
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public boolean isEmpty() {
        return rows == 0;
    }

    @Override
    public Matrix mult(Matrix m) {
        if (this.cols() != m.rows()) throw new NotMatchingMatricesMultiplicationException();

        double[][] otherMatrix = m.toArray();
        double[][] result = new double[this.rows()][m.cols()];
        for (int i = 0; i < this.rows(); i++) {
            for (int j = 0; j < m.cols(); j++) {
                result[i][j] = 0;
                for (int k = 0; k < this.cols(); k++) {
                    result[i][j] += this.matrix[i][k] * otherMatrix[k][j];
                }
            }
        }
        return new ArrayMatrix(result);
    }

    @Override
    public Matrix transpose() {
        double[][] newMatrix = new double[cols][rows];
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                newMatrix[i][j] = matrix[j][i];
            }
        }
        return new ArrayMatrix(newMatrix);
    }

    @Override
    public double[][] toArray() {
        double[][] ret = new double[rows][];
        for (int i = 0; i < rows; i++) {
            ret[i] = matrix[i].clone();
        }
        return ret;
    }

    @Override
    public String toString() {
        if (isEmpty()) return "{}";

        StringBuilder matrixBuilder = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            StringBuilder rowBuilder = new StringBuilder();
            for (int j = 0; j < cols; j++) {
                rowBuilder.append(String.format("%.2f", matrix[i][j]));
                if (j < cols - 1) rowBuilder.append("   ");
            }
            matrixBuilder.append(String.format("{ %s }", rowBuilder.toString()));
            if (i < rows - 1) matrixBuilder.append("\n");
        }

        return matrixBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof ArrayMatrix)) return false;
        ArrayMatrix other = (ArrayMatrix) o;
        if (this.rows() == other.rows() && this.cols() == other.cols()) {
            return Arrays.deepEquals(this.matrix, other.matrix);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        assert false : "hashCode not implemented";
        return 42;
    }
}
