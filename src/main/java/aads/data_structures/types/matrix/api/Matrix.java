package aads.data_structures.types.matrix.api;

public interface Matrix {
    int rows();

    int cols();

    boolean isEmpty();

    Matrix mult(final Matrix m);

    Matrix transpose();

    double[][] toArray();
}
