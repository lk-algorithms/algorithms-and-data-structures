package aads.data_structures.types.rational.implementation;

import aads.algorithms.greatest_common_divisor.api.GreatestCommonDivisor;
import aads.algorithms.greatest_common_divisor.implementation.GreatestCommonDivisorImpl;
import aads.data_structures.types.rational.api.Rational;

import java.util.Objects;

public class RationalImpl implements Rational {

    private final long numerator;
    private final long denominator;

    public RationalImpl(long numerator, long denominator) {
        GreatestCommonDivisor gcd = new GreatestCommonDivisorImpl();
        long div = gcd.gcd(numerator, denominator);
        long shortNumerator = numerator / div;
        long shortDenominator = denominator / div;

        this.numerator = shortDenominator > 0 ? shortNumerator : -shortNumerator;
        this.denominator = shortDenominator > 0 ? shortDenominator : -shortDenominator;
    }

    @Override
    public long numerator() {
        return numerator;
    }

    @Override
    public long denominator() {
        return denominator;
    }

    @Override
    public Rational plus(Rational that) {
        long newDenominator = this.denominator * that.denominator();
        long thisNumerator = this.numerator * that.denominator();
        long thatNumerator = that.numerator() * this.denominator;
        long newNumerator = thisNumerator + thatNumerator;
        return new RationalImpl(newNumerator, newDenominator);
    }

    @Override
    public Rational minus(Rational that) {
        long newDenominator = this.denominator * that.denominator();
        long thisNumerator = this.numerator * that.denominator();
        long thatNumerator = that.numerator() * this.denominator;
        long newNumerator = thisNumerator - thatNumerator;
        return new RationalImpl(newNumerator, newDenominator);
    }

    @Override
    public Rational times(Rational that) {
        long newNumerator = this.numerator * that.numerator();
        long newDenominator = this.denominator * that.denominator();
        return new RationalImpl(newNumerator, newDenominator);
    }

    @Override
    public Rational dividedBy(Rational that) {
        long newNumerator = this.numerator * that.denominator();
        long newDenominator = this.denominator * that.numerator();
        return new RationalImpl(newNumerator, newDenominator);
    }

    @Override
    public String toString() {
        return String.format("%d/%d", this.numerator, this.denominator);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;
        RationalImpl that = (RationalImpl) o;
        return this.numerator == that.numerator
                && this.denominator == that.denominator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }
}
