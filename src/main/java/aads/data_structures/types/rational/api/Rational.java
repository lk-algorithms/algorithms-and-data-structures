package aads.data_structures.types.rational.api;

public interface Rational {
    long numerator();

    long denominator();

    Rational plus(Rational that);

    Rational minus(Rational that);

    Rational times(Rational that);

    Rational dividedBy(Rational that);
}
