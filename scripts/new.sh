#!/usr/bin/env bash

set -x

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

NAME=""
LOCATION=""
LOCATION_SRC=""
LOCATION_TEST=""
WITH_EXCEPTION=""

usage() {
  cat <<EOF
  usage: $0 -n NAME -l WHERE [-e]

  This scripts creates space for new implementation.

  OPTIONS:
    -h Show this message
    -n New package name
    -l Where the package should be created
    -e With 'exception' dir
       Available options: 'algorithms', 'data_structures', 'experiments'
EOF
}

create_space() {
  mkdir -p "$LOCATION_SRC/$NAME/implementation"
  mkdir -p "$LOCATION_SRC/$NAME/api"
  if [[ -n "$WITH_EXCEPTION" ]];then
    mkdir -p "$LOCATION_SRC/$NAME/exception"
  fi
  mkdir -p "$LOCATION_TEST/$NAME"
}

main() {
  while getopts "hn:l:e" option; do
    case $option in
      h)
        usage
        exit 0
        ;;
      n)
        NAME="$OPTARG"
        ;;
      l)
        LOCATION="$OPTARG"
        ;;

      e)
        WITH_EXCEPTION="yes"
        ;;
    esac
  done

  if [[ -z "$NAME" ]];then
    echo -e "ERROR: Passing NAME is mandatory\n"
    usage
    exit 1
  fi

  if [[ -z "$LOCATION" ]];then
    echo -e "ERROR: Passing LOCATION is mandatory\n"
    usage
    exit 1
  fi

  case "$LOCATION" in
  "algorithms")
    LOCATION_SRC="$SCRIPT_DIR/../src/main/java/aads/algorithms"
    LOCATION_TEST="$SCRIPT_DIR/../src/test/java/aads/algorithms"
    ;;
  "data_structures")
    LOCATION_SRC="$SCRIPT_DIR/../src/main/java/aads/data_structures"
    LOCATION_TEST="$SCRIPT_DIR/../src/test/java/aads/data_structures"
    ;;
  "experiments")
    LOCATION_SRC="$SCRIPT_DIR/../src/main/java/aads/experiments"
    LOCATION_TEST="$SCRIPT_DIR/../src/test/java/aads/experiments"
    ;;
  *)
    echo -e "ERROR: LOCATION must be 'algorithms', 'data_structures' or 'experiments'\n"
    usage
    exit 1
  esac

  create_space
}

main "$@"