#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
SRC_DIR="$PROJECT_DIR/src"
SRC_ONLY=""

usage() {
  cat <<EOF
  usage: $0 [-s]

  This script Counts Lines Of Code.

  OPTIONS:
    -h Show this message
    -s Run CLOC over src dir only
EOF
}

main() {
  while getopts "hs" option; do
    case $option in
      h)
        usage
        exit 0
        ;;
      s)
        SRC_ONLY="yes"
        ;;
    esac
  done

  if [[ -n "$SRC_ONLY" ]]; then
    cloc "$SRC_DIR"
  else
    cloc "$PROJECT_DIR"
  fi
}

main "$@"