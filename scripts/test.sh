#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
SPOTBUGS_MAIN_REPORT="$PROJECT_DIR/build/reports/spotbugs/main.xml"
SPOTBUGS_TEST_REPORT="$PROJECT_DIR/build/reports/spotbugs/test.xml"

if $PROJECT_DIR/gradlew check; then
    echo "Tests completed successfully"
    exit 0
else
    if [ -f $SPOTBUGS_MAIN_REPORT ]; then
        echo "Spotbugs main report"
        cat $SPOTBUGS_MAIN_REPORT
    fi

    if [ -f $SPOTBUGS_TEST_REPORT ]; then
        echo "Spotbugs test report"
        cat $SPOTBUGS_TEST_REPORT
    fi
    exit 1
fi